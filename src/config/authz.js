const { expressjwt: jwt } = require('express-jwt');
const jwksRsa = require('jwks-rsa');
// Config for auth0
const checkJwt = jwt({
  // Dynamically provide a signing key based on the kid in the header and the signing keys provided by the JWKS endpoint
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://dev-7rsdwh1i.us.auth0.com/.well-known/jwks.json`,
  }),
  // Validate the audience and the issuer
  audience: process.env.NODE_ENV === 'production' ? 'https://cashcashe-backend.vercel.app/' : 'http://localhost:3500', // replace with your API's audience, available at Dashboard > APIs
  issuer: 'https://dev-7rsdwh1i.us.auth0.com/',
  algorithms: ['RS256'],
});

module.exports = checkJwt;
