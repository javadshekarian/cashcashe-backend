const dotenv = require('dotenv');
const path = require('path');
const Joi = require('joi');

dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string().valid('production', 'development', 'test').required(),
    PORT: Joi.number().default(3000),
    MONGODB_URL: Joi.string().required().description('Mongo DB url'),
    SENTRY_DSN: Joi.string().required().description('Sentry DSN URL'),
    AUTH_DOMAIN: Joi.string().required().description('Auth0 app specific domain'),
    AUTH_CLIENT_ID: Joi.string().required().description('Auth0 client ID'),
    AUTH_CLIENT_SECRET: Joi.string().required().description('Auth0 client secret'),
    JWT_SECRET: Joi.string().required().description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number().default(30).description('minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number().default(30).description('days after which refresh tokens expire'),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description('minutes after which reset password token expires'),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description('minutes after which verify email token expires'),
    SMTP_HOST: Joi.string().description('server that will send the emails'),
    SMTP_PORT: Joi.number().description('port to connect to the email server'),
    SMTP_USERNAME: Joi.string().description('username for email server'),
    SMTP_PASSWORD: Joi.string().description('password for email server'),
    EMAIL_FROM: Joi.string().description('the from field in the emails sent by the app'),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongoose: {
    url: envVars.MONGODB_URL + (envVars.NODE_ENV === 'test' ? '-test' : ''),
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  tester_uid: '6305fb1e988e656154a45bb4',
  sentry_dsn: envVars.SENTRY_DSN,
  auth: {
    domain: envVars.AUTH_DOMAIN,
    clientId: envVars.AUTH_CLIENT_ID,
    clientSecret: envVars.AUTH_CLIENT_SECRET,
    baseURL: envVars.AUTH_BASE_URL,
  },
  paystack_secret: envVars.PAYSTACK_SECRET,
  games: {
    gameIDs: ['ER', 'BBI', 'KN', 'BBE', 'WF', 'SL'],
    gamesList: [
      {
        id: 'ER',
        name: 'European Roulette',
      },
      {
        id: 'BBI',
        name: '75 Ball Bingo',
      },
      {
        id: 'KN',
        name: 'Keno',
      },
      {
        id: 'BBE',
        name: 'Bulls & Bears',
      },
      {
        id: 'WF',
        name: 'Wheel of Fortune',
      },
    ],
    minimumGameBalance: 100,
    gamePlayCost: 100,
  },
  jwt: {
    secret: envVars.JWT_SECRET,
    secretTfa: envVars.JWT_SECRET_TFA,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: envVars.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: envVars.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD,
      },
    },
    from: envVars.EMAIL_FROM,
  },
  dbLoc: path.join(__dirname, '../../uploads/db.json'),
  transactionInstanctes: ['GMI', 'IPG', 'ST'],
  recaptchaKey: envVars.GOOGLE_RECAPTCHA_SECRET,
};
