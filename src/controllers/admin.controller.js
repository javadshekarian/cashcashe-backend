/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
const _ = require('lodash');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const bcrypt = require('bcryptjs');
const moment = require('moment');
const MasterPassword = require('../models/masterPassword.model');
const {
  adminService,
  transactionService,
  userService,
  ticketService,
  contactService,
  profileService,
} = require('../services');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');
const User = require('../models/user.model');

const getDashboardData = catchAsync(async (req, res) => {
  const games = await adminService.getTodayGames();
  const transactions = await adminService.getTodayTransactions();

  return res.status(200).send({
    success: true,
    data: { games, transactions },
  });
});

const queryTransactions = catchAsync(async (req, res) => {
  const { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  if (filter && filter.userName) {
    const _user = await User.findOne({
      userName: filter.userName,
    });

    if (_user) {
      filter.assocUser = _user._id.toString();
      delete filter.userName;
    } else {
      filter.assocUser = 'UNKNOWN';
      delete filter.userName;
    }
  }

  if (filter && filter.createdAt) {
    filter.createdAt = {
      $gte: moment(filter.createdAt).startOf('day').toDate(),
      $lte: moment(filter.createdAt).add(1, 'day').startOf('day').toDate(),
    };
  }
  const result = await adminService.queryTransactions(filter, {
    sortBy: sortBy || '',
    populate: populate || '',
    limit: limit || 10,
    page: page || 1,
  });

  const _res = [];

  for (let i = 0; i < result.results.length; i++) {
    const item = result.results[i];
    const _user = await userService.getUserById(item.assocUser);
    _res.push({
      ...item.toJSON(),
      assocUserName: _user.userName,
      email: _user.email,
    });
  }

  return res.status(200).send({
    success: true,
    results: _res,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

const queryUsers = catchAsync(async (req, res) => {
  const { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  const result = await adminService.queryUsers(filter, {
    sortBy: sortBy || '',
    populate: populate || '',
    limit: limit || 10,
    page: page || 1,
  });

  return res.status(200).send({
    success: true,
    results: result.results,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

const getUserInfo = catchAsync(async (req, res) => {
  const { userId } = req.params;

  const _user = await userService.getUserById(userId);

  if (!_user) {
    throw new ApiError(200, 'User not found');
  }

  const { games, transactions, tickets, agents } = _user;
  const _agents = await adminService.getAgents(agents);
  const doc = _.pick(_user.toJSON(), [
    'userName',
    'nameString',
    'name',
    'lastName',
    'status',
    'email',
    'id',
    'about',
    'address',
    'country',
    'phone',
    'role',
  ]);
  const { balance } = await profileService.getUserBalance(_user.email);

  const stats = {
    games: games.length,
    transactions: transactions.length,
    tickets: tickets.length,
    agents: _agents || [],
    balance,
  };

  const gameStats = {
    total: games.length,
    won: games.filter((x) => x.result === 2).length,
    lost: games.filter((x) => x.result === 1).length,
    recent: _.reverse(games).slice(0, 3),
  };

  const total = transactions.filter((x) => x.transactionInstance === 'IPG');
  const withdraw = transactions.filter((x) => x.type === 2 && x.transactionInstance === 'IPG');
  const deposit = transactions.filter((x) => x.type === 1 && x.transactionInstance === 'IPG');
  const fees = transactions.filter((x) => x.type === 2 && x.transactionInstance === 'GMI');
  const awards = transactions.filter((x) => x.type === 1 && x.transactionInstance === 'GMI');

  const transactionStats = {
    turnover: total.reduce((acc, i) => acc + i.amount, 0),
    deposit: deposit.reduce((acc, i) => acc + i.amount, 0),
    withdraw: withdraw.reduce((acc, i) => acc + i.amount, 0),
    fees: fees.reduce((acc, i) => acc + i.amount, 0),
    awards: awards.reduce((acc, i) => acc + i.amount, 0),
    recent: transactions.slice(0, 3),
  };

  const openTickets = tickets.filter((x) => x.status === 3);
  const closedTickets = tickets.filter((x) => x.status === 3);

  const ticketStats = {
    total: tickets.length,
    open: openTickets.length,
    closed: closedTickets.length,
    recent: tickets.slice(0, 3),
  };

  return res.status(200).send({
    success: true,
    user: {
      ...doc,
      stats,
      gameStats,
      transactionStats,
      ticketStats,
    },
  });
});

const queryAgents = catchAsync(async (req, res) => {
  const { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  const result = await adminService.queryUsers(
    {
      ...filter,
      role: 'agent',
    },
    {
      sortBy: sortBy || '',
      populate: populate || '',
      limit: limit || 10,
      page: page || 1,
    }
  );

  return res.status(200).send({
    success: true,
    results: result.results,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

const querySupportTickets = catchAsync(async (req, res) => {
  const { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  if (filter && filter.userName) {
    const _user = await User.findOne({
      userName: filter.userName,
    });

    if (_user) {
      filter.assocUser = _user._id.toString();
      delete filter.userName;
    } else {
      filter.assocUser = 'UNKNOWN';
      delete filter.userName;
    }
  }

  if (filter && filter.createdAt) {
    filter.createdAt = {
      $gte: moment(filter.createdAt).startOf('day').toDate(),
      $lte: moment(filter.createdAt).add(1, 'day').startOf('day').toDate(),
    };
  }
  const result = await adminService.querySupportTickets(filter, {
    sortBy: sortBy || '',
    populate: populate || '',
    limit: limit || 10,
    page: page || 1,
  });

  const _res = [];

  for (let i = 0; i < result.results.length; i++) {
    const item = result.results[i];
    const _user = await userService.getUserById(item.associatedUser);
    _res.push({
      ...item.toJSON(),
      assocUserName: _user.userName,
      email: _user.email,
    });
  }

  return res.status(200).send({
    success: true,
    results: _res,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

const queryGames = catchAsync(async (req, res) => {
  const { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  if (filter && filter.userName) {
    const _user = await User.findOne({
      userName: filter.userName,
    });

    if (_user) {
      filter.player = _user._id.toString();
      delete filter.userName;
    } else {
      filter.player = 'UNKNOWN';
      delete filter.userName;
    }
  }

  if (filter && filter.createdAt) {
    filter.createdAt = {
      $gte: moment(filter.createdAt).startOf('day').toDate(),
      $lte: moment(filter.createdAt).add(1, 'day').startOf('day').toDate(),
    };
  }

  const result = await adminService.queryGames(filter, {
    sortBy: sortBy || '',
    populate: populate || '',
    limit: limit || 10,
    page: page || 1,
  });

  const _res = [];

  for (let i = 0; i < result.results.length; i++) {
    const item = result.results[i];
    const _user = await userService.getUserById(item.player);
    _res.push({
      ...item.toJSON(),
      assocUserName: _user.userName,
    });
  }

  return res.status(200).send({
    success: true,
    results: _res,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

const createSetting = catchAsync(async (req, res) => {
  await adminService.createSetting(req.body);

  return res.status(200).send({
    success: true,
    message: 'Setting has been created',
  });
});

const getCurrentSetting = catchAsync(async (req, res) => {
  const _setting = await adminService.getSetting();

  return res.status(200).send({
    success: true,
    setting: _setting,
  });
});

const debitUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  const { instance, amount } = req.body;

  await transactionService.createTransaction({
    type: 2,
    amount,
    transactionInstance: instance,
    absAmount: amount,
    currency: 'NGN',
    fees: 0,
    assocUser: userId,
    reference: 0,
    paymentPayload: {},
    paymentVerificationResponse: {},
  });

  return res.status(200).send({
    success: true,
    message: 'Transaction created.',
  });
});

const creditUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  const { instance, amount } = req.body;

  await transactionService.createTransaction({
    type: 1,
    amount,
    transactionInstance: instance,
    absAmount: amount,
    currency: 'NGN',
    fees: 0,
    assocUser: userId,
    reference: 0,
    paymentPayload: {},
    paymentVerificationResponse: {},
  });

  return res.status(200).send({
    success: true,
    message: 'Transaction created.',
  });
});

const updateUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  const state = {
    hasInfo: false,
  };
  const { name, lastName, country, phone, address, about, agents, role } = req.body;

  if (phone && phone.length > 0) {
    const isValid = phoneUtil.isValidNumberForRegion(phoneUtil.parse(phone, country), country);
    if (!isValid) {
      return res.status(200).send({
        success: false,
        message: 'Phone number is invalid',
      });
    }
  }
  if (name && lastName && country && phone && address) {
    state.hasInfo = true;
  }

  await userService.updateUserById(
    userId,
    _.omitBy(
      {
        name,
        lastName,
        country,
        phone,
        address,
        about,
        agents,
        role,
        hasInfo: state.hasInfo,
      },
      (x) => x === '' || _.isNil(x)
    )
  );

  return res.status(200).send({
    success: true,
    message: 'Profile has been updated',
  });
});

const tempBanUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  await userService.updateUserById(userId, {
    status: 1,
  });
  return res.status(200).send({
    success: true,
    message: 'User has been temporary banned.',
  });
});

const permanentlyBan = catchAsync(async (req, res) => {
  const { userId } = req.params;
  await userService.updateUserById(userId, {
    status: 2,
  });
  return res.status(200).send({
    success: true,
    message: 'User has been permanently banned.',
  });
});

const unbanUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  await userService.updateUserById(userId, {
    status: 0,
  });
  return res.status(200).send({
    success: true,
    message: 'User has been unbanned.',
  });
});

const setMasterPassowrd = catchAsync(async (req, res) => {
  const { currentPassword, password } = req.body;
  const _current = await MasterPassword.findOne({
    isActive: true,
  });

  const isValid = await bcrypt.compare(currentPassword, _current.password);

  if (!isValid) {
    throw new ApiError(200, 'Current master password is incorrect');
  }

  _current.isActive = false;
  _current.markModified('isActive');
  await _current.save();
  const hashed = await bcrypt.hash(password, 8);
  const _new = new MasterPassword({
    password: hashed,
    isActive: true,
  });

  await _new.save();

  return res.status(200).send({
    success: true,
    message: 'Master Password has been set',
  });
});

// const createSupportTicket = catchAsync(async (req, res) => {});

const closeTicket = catchAsync(async (req, res) => {
  const { ticketNumber } = req.params;
  const ticket = await ticketService.findTicketByNumber(ticketNumber);

  if (!ticket) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  const user = await userService.getUserById(ticket.associatedUser);
  const userTicket = _.filter(user.tickets, (x) => x.ticketNumber === ticketNumber)[0];
  const ticketIndex = _.findIndex([...user.tickets], (x) => x.ticketNumber === ticketNumber);

  ticket.status = 3;
  userTicket.status = 3;

  user.tickets[ticketIndex] = { ...userTicket };
  ticket.markModified('status');

  await ticket.save();
  await user.save();

  return res.status(200).send({
    success: true,
    message: `Ticket ${ticketNumber} has been closed`,
  });
});

const answerTicket = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const sender = await userService.getUserByEmail(email);
  const { ticketNumber } = req.params;
  const { description } = req.body;

  const ticket = await ticketService.findTicketByNumber(ticketNumber);

  if (!ticket) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }
  const user = await userService.getUserById(ticket.associatedUser);
  const ticketMessages = ticket.messages;
  const userTicket = _.filter(user.tickets, (x) => x.ticketNumber === ticketNumber)[0];
  const userMessages = userTicket.messages;
  const ticketIndex = _.findIndex([...user.tickets], (x) => x.ticketNumber === ticketNumber);

  ticketMessages.push({
    sender: sender.userName,
    senderName: 'Cashcashe Support',
    date: new Date().toISOString(),
    message: description,
    isAdmin: true,
  });

  userMessages.push({
    sender: sender.userName,
    senderName: 'Cashcashe Support',
    date: new Date().toISOString(),
    message: description,
    isAdmin: true,
  });

  ticket.status = 2;
  userTicket.status = 2;

  ticket.messages = [...ticketMessages];
  ticket.lastMessageDate = new Date().toISOString();
  userTicket.lastMessageDate = new Date().toISOString();
  userTicket.messages = [...userMessages];
  user.tickets[ticketIndex] = { ...userTicket };

  await ticket.save();
  await user.save();

  return res.status(200).send({
    success: true,
    message: 'Your answer has been submitted',
  });
});

const queryContactMessages = catchAsync(async (req, res) => {
  const { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  if (filter && filter.createdAt) {
    filter.createdAt = {
      $gte: moment(filter.createdAt).startOf('day').toDate(),
      $lte: moment(filter.createdAt).add(1, 'day').startOf('day').toDate(),
    };
  }

  const result = await contactService.queryContactMessages(filter, {
    sortBy: sortBy || '',
    populate: populate || '',
    limit: limit || 10,
    page: page || 1,
  });

  return res.status(200).send({
    success: true,
    results: result.results,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

module.exports = {
  getDashboardData,
  queryTransactions,
  queryUsers,
  queryAgents,
  querySupportTickets,
  queryGames,
  createSetting,
  getCurrentSetting,
  debitUser,
  creditUser,
  updateUser,
  tempBanUser,
  permanentlyBan,
  unbanUser,
  answerTicket,
  closeTicket,
  queryContactMessages,
  setMasterPassowrd,
  getUserInfo,
};
