const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const moment = require('moment');
const PlayToken = require('../models/playToken.model');
const { transactionService, userService, profileService, adminService } = require('../services');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

const updateUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  const user = await userService.getUserById(userId);
  const state = {
    hasInfo: false,
  };
  const { name, lastName, country, phone, address, about, profilePic, settings } = req.body;

  if (phone && phone.length > 0) {
    const isValid = phoneUtil.isValidNumberForRegion(phoneUtil.parse(phone, country), country);
    if (!isValid) {
      return res.status(200).send({
        success: false,
        message: 'Phone number is invalid',
      });
    }
  }
  if (name && lastName && country && phone && address) {
    state.hasInfo = true;
  }

  if (profilePic) {
    let _profilePicture = user.profilePicture;

    if (!_profilePicture) {
      _profilePicture = {
        url: '',
        changeLog: [],
      };
    }

    _profilePicture.url = profilePic;
    _profilePicture.changeLog.push({ date: new Date().toISOString(), url: profilePic });
    user.profilePicture = _profilePicture;
  }

  await userService.updateUserById(userId, {
    name,
    lastName,
    country,
    phone,
    address,
    about,
    settings,
    hasInfo: state.hasInfo,
    profilePicture: user.profilePicture,
  });

  return res.status(200).send({
    success: true,
    message: 'Profile has been updated',
  });
});

const preCheckCredit = catchAsync(async (req, res) => {
  const { userEmail, amount } = req.body;
  const _user = await userService.getUserByEmail(userEmail);
  if (!_user) {
    throw new ApiError(200, 'There is no user with entered information, Please double check user email');
  }

  if (_user.role !== 'user') {
    throw new ApiError(200, `You don't have permission to transfer money to non-user accounts`);
  }

  const { nameString, email, userName } = _user;

  return res.status(200).send({
    success: true,
    data: {
      nameString,
      email,
      userName,
      amount,
    },
  });
});

const creditUser = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const _agent = await userService.getUserByEmail(email);
  if (!_agent) {
    throw new ApiError(200, 'Agent not found');
  }
  const { userEmail, amount } = req.body;
  const _user = await userService.getUserByEmail(userEmail);
  if (!_user) {
    throw new ApiError(200, 'There is no user with entered information, Please double check user email');
  }

  if (_user.role !== 'user') {
    throw new ApiError(200, `You don't have permission to transfer money to non-user accounts`);
  }

  const { balance } = profileService.getUserBalance(_agent.email);
  if (balance < amount) {
    throw new ApiError(
      200,
      `You don't have sufficient balance to perform this transaction, Please topup your account and try again`
    );
  }

  const _originTransaction = await transactionService.createTransaction({
    type: 2,
    amount,
    transactionInstance: 'P2P',
    absAmount: amount,
    event: {
      date: new Date().toLocaleString(),
      info: `Transfer Transaction from ${_agent.email} (Agent) to ${_user.email} (User)`,
      status: 1,
    },
    fees: 0,
    reference: 'INTERNAL',
    currency: 'NGN',
    assocUser: _agent._id.toString(),
    paymentPayload: { description: 'This Payment is a Agent to user Transaction' },
    paymentVerificationResponse: {},
  });

  const _destinationTransaction = await transactionService.createTransaction({
    type: 1,
    amount,
    absAmount: amount,
    transactionInstance: 'P2P',
    fees: 0,
    event: {
      date: new Date().toLocaleString(),
      info: `Transfer Transaction from ${_agent.email} (Agent) to ${_user.email} (User)`,
      status: 1,
    },
    currency: 'NGN',
    reference: 'INTERNAL',
    assocUser: _user._id.toString(),
    paymentPayload: { description: 'This Payment is a Agent to user Transaction' },
    paymentVerificationResponse: {},
  });

  let { agents } = _user;
  if (!agents) agents = [];
  if (agents.indexOf(_agent._id.toString()) < 0) {
    agents.push(_agent._id.toString());
    _user.agents = agents;
    _user.markModified('agents');
    await _user.save();
  }

  return res.status(200).send({
    success: true,
    message: `Successfully Transferred ${amount} NGN to ${userEmail}`,
    infos: {
      invoiceNumber: _originTransaction.invoiceNumber,
      receiverInvoiceNumber: _destinationTransaction.invoiceNumber,
      amount,
      origin: _agent.userName,
      destination: _user.userName,
      date: Date.now(),
    },
  });
});

const getPlayToken = catchAsync(async (req, res) => {
  const { email, targetGame } = req.body;
  const { email: agentEmail } = req.auth.cshInfo;
  const _user = await userService.getUserByEmail(email);
  const _agent = await userService.getUserByEmail(agentEmail);

  if (!_user) {
    throw new ApiError(200, 'There is no user with entered information, Please double check user email');
  }
  const { agents, role } = _user;
  if (role !== 'user') {
    throw new ApiError(200, `You don't have permission to play as non-user accounts`);
  }

  if (!agents) {
    throw new ApiError(200, `You don't have permission to play as this account`);
  }

  if (agents.indexOf(_agent._id.toString()) < -1) {
    throw new ApiError(200, `You don't have permission to play as this account`);
  }

  const token = new PlayToken({
    targetGame,
    player: _agent._id.toString(),
    targetUser: _user._id.toString(),
  });

  await token.save();

  return res.status(200).send({
    success: true,
    message: 'Play Ticket issued, Redirecting to the game page',
    token: token._id.toString(),
  });
});

const queryTransactions = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  let { filter } = req.body;
  const options = req.body.options || {};
  const { sortBy, populate, limit, page } = options;

  const _agentId = await userService.getUserIdByEmail(email);
  if (!filter) filter = {};
  filter.assocUser = _agentId.toString();

  if (filter && filter.createdAt) {
    filter.createdAt = {
      $gte: moment(filter.createdAt).startOf('day').toDate(),
      $lte: moment(filter.createdAt).add(1, 'day').startOf('day').toDate(),
    };
  }
  const result = await adminService.queryTransactions(filter, {
    sortBy: sortBy || '',
    populate: populate || '',
    limit: limit || 10,
    page: page || 1,
  });

  return res.status(200).send({
    success: true,
    results: result.results,
    page: result.page,
    limit: result.limit,
    totalPages: result.totalPages,
    totalResults: result.totalResults,
  });
});

module.exports = { updateUser, creditUser, getPlayToken, preCheckCredit, queryTransactions };
