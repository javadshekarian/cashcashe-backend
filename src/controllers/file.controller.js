const httpStatus = require('http-status');
const fs = require('fs');
const _ = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { UploadModel } = require('../models');

const uploadFile = catchAsync(async (req, res) => {
  const { file } = req;
  const types = ['image/jpeg', 'image/png'];
  const _valid = _.filter(types, (x) => x === file.mimetype);
  if (_valid.length < 1) {
    return res.status(403).send({
      type: 'Bad Request',
      msg: 'Invalid file format , allowed formats : .JPG and .PNG',
    });
  }
  const files = [file];

  if (!files) {
    return res.status(httpStatus.BAD_REQUEST).send({
      success: false,
      message: 'Empty file',
    });
  }

  // convert images into base64 encoding
  const imgArray = files.map((imgfile) => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const img = fs.readFileSync(imgfile.path);
    const encodeImage = img.toString('base64');

    return encodeImage;
  });

  const finalImg = {
    fileName: files[0].originalname.replace(/\s+/g, '-').toLowerCase(),
    contentType: files[0].mimetype,
    imageBase64: imgArray[0],
  };

  const newUpload = new UploadModel(finalImg);

  await newUpload.save();

  // eslint-disable-next-line security/detect-non-literal-fs-filename

  files.map((imgfile) => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    return fs.unlinkSync(imgfile.path);
  });

  return res.status(httpStatus.OK).send({
    success: true,
    fileId: newUpload._id,
    fileName: newUpload.fileName,
  });
});

const viewFile = catchAsync(async (req, res) => {
  const image = await UploadModel.findById({
    _id: req.params.fileId,
    fileName: req.params.fileName,
  });

  if (!image) {
    return res.status(404).send('404 Not Found');
  }

  const img = Buffer.from(image.imageBase64, 'base64');

  res.writeHead(200, {
    'Content-Type': `${image.contentType}`,
    'Content-Length': img.length,
  });
  res.end(img);
});

module.exports = {
  uploadFile,
  viewFile,
};
