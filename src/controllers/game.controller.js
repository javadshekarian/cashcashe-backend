/* eslint-disable camelcase */
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { gameService, profileService, userService } = require('../services');
const config = require('../config/config');
const ApiError = require('../utils/ApiError');
const PlayToken = require('../models/playToken.model');

const getGameStatus = catchAsync(async (req, res) => {
  const { id } = req.params;

  const game = await gameService.getGameById(id);

  if (game) {
    const { isFinished } = game;

    if (isFinished) {
      const { result, return_amount, calculatedPayload } = game;

      return res.status(200).send({
        success: true,
        isFinished,
        result,
        return_amount,
        resultPayload: calculatedPayload,
      });
    }

    return res.status(200).send({
      success: true,
      isFinished,
    });
  }

  return res.status(200).send({
    success: false,
    message: 'Game not found',
  });
});

const playKeno = catchAsync(async (req, res) => {
  const { payload } = req.body;
  const { playToken } = req.query;
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (playToken) {
    const _token = await PlayToken.findById(playToken);

    if (!_token) {
      throw new ApiError(200, 'Access Denied. Invalid Play Token provided');
    }

    if (_token.player !== user._id.toString()) {
      throw new ApiError(200, 'Access Denied. You do not have access to play with this token');
    }

    const game = await gameService.createGame(
      {
        instance: 'KN',
        player: _token.targetUser,
        userPayload: payload,
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  } else {
    const game = await gameService.createGame(
      {
        instance: 'KN',
        player: user._id,
        userPayload: payload,
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  }
});

const playBingo = catchAsync(async (req, res) => {
  const { payload } = req.body;
  const { playToken } = req.query;
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (playToken) {
    const _token = await PlayToken.findById(playToken);

    if (!_token) {
      throw new ApiError(200, 'Access Denied. Invalid Play Token provided');
    }

    if (_token.player !== user._id.toString()) {
      throw new ApiError(200, 'Access Denied. You do not have access to play with this token');
    }

    const game = await gameService.createGame(
      {
        instance: 'BBI',
        player: _token.targetUser,
        userPayload: payload,
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  } else {
    const game = await gameService.createGame(
      {
        instance: 'BBI',
        player: user._id,
        userPayload: payload,
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  }
});

const playBullBear = catchAsync(async (req, res) => {
  const { playToken } = req.query;
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (playToken) {
    const _token = await PlayToken.findById(playToken);

    if (!_token) {
      throw new ApiError(200, 'Access Denied. Invalid Play Token provided');
    }

    if (_token.player !== user._id.toString()) {
      throw new ApiError(200, 'Access Denied. You do not have access to play with this token');
    }

    const game = await gameService.createGame(
      {
        instance: 'BBE',
        player: _token.targetUser,
        userPayload: [],
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  } else {
    const game = await gameService.createGame(
      {
        instance: 'BBE',
        player: user._id,
        userPayload: [],
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  }
});

const playRoulette = catchAsync(async (req, res) => {
  const { payload } = req.body;
  const { playToken } = req.query;
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (playToken) {
    const _token = await PlayToken.findById(playToken);

    if (!_token) {
      throw new ApiError(200, 'Access Denied. Invalid Play Token provided');
    }

    if (_token.player !== user._id.toString()) {
      throw new ApiError(200, 'Access Denied. You do not have access to play with this token');
    }

    const game = await gameService.createGame(
      {
        instance: 'ER',
        player: _token.targetUser,
        userPayload: payload,
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  } else {
    const game = await gameService.createGame(
      {
        instance: 'ER',
        player: user._id,
        userPayload: payload,
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  }
});

const playWheel = catchAsync(async (req, res) => {
  const { playToken } = req.query;
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (playToken) {
    const _token = await PlayToken.findById(playToken);

    if (!_token) {
      throw new ApiError(200, 'Access Denied. Invalid Play Token provided');
    }

    if (_token.player !== user._id.toString()) {
      throw new ApiError(200, 'Access Denied. You do not have access to play with this token');
    }

    const game = await gameService.createGame(
      {
        instance: 'WF',
        player: _token.targetUser,
        userPayload: [],
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  } else {
    const game = await gameService.createGame(
      {
        instance: 'WF',
        player: user._id,
        userPayload: [],
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  }
});

const playSlot = catchAsync(async (req, res) => {
  const { playToken } = req.query;
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (playToken) {
    const _token = await PlayToken.findById(playToken);

    if (!_token) {
      throw new ApiError(200, 'Access Denied. Invalid Play Token provided');
    }

    if (_token.player !== user._id.toString()) {
      throw new ApiError(200, 'Access Denied. You do not have access to play with this token');
    }

    const game = await gameService.createGame(
      {
        instance: 'SL',
        player: _token.targetUser,
        userPayload: [],
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  } else {
    const game = await gameService.createGame(
      {
        instance: 'SL',
        player: user._id,
        userPayload: [],
      },
      user._id
    );

    res.status(200).send({
      success: true,
      message: 'Request submitted, contact result API',
      tid: game._id,
    });

    await gameService.handleResults([game]);
  }
});

const canUserPlay = catchAsync(async (req, res) => {
  // const uid = config.tester_uid;
  const { email } = req.auth.cshInfo;
  const { balance } = await profileService.getUserBalance(email);

  if (balance < config.games.minimumGameBalance) {
    throw new ApiError(200, `You don't have sufficient balance to play this game, topup your account and try again`);
  }

  return res.status(httpStatus.OK).send({
    success: true,
    message: 'Have fun!',
  });
});

module.exports = {
  playKeno,
  playBingo,
  playBullBear,
  playRoulette,
  playWheel,
  playSlot,
  getGameStatus,
  canUserPlay,
};
