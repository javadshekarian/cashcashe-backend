/* eslint-disable no-await-in-loop */
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const httpStatus = require('http-status');
const config = require('../config/config');
const { gameRuleService } = require('../services');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const MasterPassword = require('../models/masterPassword.model');

const setRules = catchAsync(async (req, res) => {
  const { rules, password } = req.body;

  const _masterPassword = await MasterPassword.findOne({
    isActive: true,
  });

  if (!_masterPassword) {
    throw new ApiError(200, 'Access Denied, Try again');
  }

  const isValid = await bcrypt.compare(password, _masterPassword.password);

  if (!isValid) {
    throw new ApiError(200, 'Access Denied, Try again');
  }

  gameRuleService.customRuleValidations(rules);

  for (let i = 0; i < rules.length; i += 1) {
    await gameRuleService.deactivateCurrentRule(rules[i].targetGame);
    await gameRuleService.createGameRule({
      ...rules[i],
      createdBy: config.tester_uid,
    });
  }

  return res.status(200).send({
    success: true,
    message: 'Rules have been set',
  });
});

const getRules = catchAsync(async (req, res) => {
  const { password } = req.params;

  const _masterPassword = await MasterPassword.findOne({
    isActive: true,
  });

  if (!_masterPassword) {
    throw new ApiError(200, 'Access Denied, Try again');
  }

  const isValid = await bcrypt.compare(password, _masterPassword.password);

  if (!isValid) {
    throw new ApiError(200, 'Access Denied, Try again');
  }

  const rules = await gameRuleService.getAllRules();

  return res.status(200).send({
    success: true,
    rules,
  });
});

const getWheelOfFortuneBase = catchAsync(async (req, res) => {
  const rules = await gameRuleService.getAllRules();

  const wheel = rules.filter((x) => x.targetGame === 'WF')[0];

  if (!wheel) {
    throw new ApiError(httpStatus.OK, 'Could not complete your request, please try again later');
  }

  return res.status(200).send({
    success: true,
    prizes: _.union(
      [{ index: 0, value: 0 }],
      wheel.rulePrizes.reduce(
        (acc, item, index) =>
          acc.concat({
            index: index + 1,
            value: item.prizeValue,
          }),
        []
      )
    ),
  });
});

module.exports = {
  setRules,
  getRules,
  getWheelOfFortuneBase,
};
