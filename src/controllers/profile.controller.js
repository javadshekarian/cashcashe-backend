const httpStatus = require('http-status');
const speakeasy = require('speakeasy');
const jwt = require('jsonwebtoken');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const _ = require('lodash');
const { default: axios } = require('axios');
const catchAsync = require('../utils/catchAsync');
const { profileService, userService, ticketService, transactionService } = require('../services');
const getPaginatedItems = require('../utils/paginate');
const config = require('../config/config');
const ApiError = require('../utils/ApiError');
const Withdraw = require('../models/withdrawRequest.model');

const userDashboardInfo = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;

  const dashboardInfo = await profileService.getDashboardInfo(email);

  return res.status(httpStatus.OK).send({
    success: true,
    dashboardInfo,
  });
});

const userTransactions = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { page } = req.query;

  const transactions = await profileService.getProfileTransactions(email);

  return res.status(httpStatus.OK).send({
    success: true,
    transactions: getPaginatedItems(transactions, page, 10),
  });
});

const userDeposits = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { page } = req.query;
  const deposits = await profileService.getProfileDeposits(email);

  return res.status(httpStatus.OK).send({
    success: true,
    deposits: getPaginatedItems(deposits, page, 10),
  });
});

const userWithdraws = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { page } = req.query;
  const withdraws = await profileService.getProfileWithdraws(email);

  return res.status(httpStatus.OK).send({
    success: true,
    withdraws: getPaginatedItems(withdraws, page, 10),
  });
});

const userGames = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { page } = req.query;
  const games = await profileService.getProfileGames(email);

  return res.status(httpStatus.OK).send({
    success: true,
    games: getPaginatedItems(_.reverse(games), page, 10),
  });
});

const userDetail = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;

  const info = await profileService.getUserDetail(email);

  return res.status(httpStatus.OK).send({
    success: true,
    info,
  });
});

const updateUserInfo = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);
  const state = {
    hasInfo: false,
  };
  const { name, lastName, country, phone, address, about, profilePic } = req.body;

  if (phone && phone.length > 0) {
    const isValid = phoneUtil.isValidNumberForRegion(phoneUtil.parse(phone, country), country);
    if (!isValid) {
      return res.status(httpStatus.OK).send({
        success: false,
        message: 'Phone number is invalid',
      });
    }
  }
  if (name && lastName && country && phone && address) {
    state.hasInfo = true;
  }

  if (profilePic) {
    let _profilePicture = user.profilePicture;

    if (!_profilePicture) {
      _profilePicture = {
        url: '',
        changeLog: [],
      };
    }

    _profilePicture.url = profilePic;
    _profilePicture.changeLog.push({ date: new Date().toISOString(), url: profilePic });
    user.profilePicture = _profilePicture;
  }

  await userService.updateUserByEmail(email, {
    name,
    lastName,
    country,
    phone,
    address,
    about,
    hasInfo: state.hasInfo,
    profilePicture: user.profilePicture,
  });

  return res.status(httpStatus.OK).send({
    success: true,
    message: 'Profile has been updated',
  });
});

const getUserTfaState = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  return res.status(200).send({
    success: true,
    tfa_state: user.tfa_active,
  });
});

const enableUserTfa = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  if (user.tfa_active) {
    return res.status(200).send({
      success: false,
      message: 'Two factor verification is already enabled',
    });
  }
  const secret = speakeasy.generateSecret();

  user.tfa = secret;

  await user.save();

  return res.status(200).send({
    success: true,
    tfa_secret: secret.base32,
  });
});

const verifyUserTfa = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { token } = req.body;

  if (!token) {
    return res.status(200).send({
      success: false,
      message: 'Token is required for Two step authentication verification',
    });
  }
  const user = await userService.getUserByEmail(email);

  if (user.tfa_active) {
    return res.status(200).send({
      success: false,
      message: 'Two factor verification is already enabled',
    });
  }

  const verified = speakeasy.totp.verify({
    secret: user.tfa.base32,
    encoding: 'base32',
    token,
  });

  if (verified) {
    user.tfa_active = true;

    await user.save();

    const tfaToken = jwt.sign(
      {
        iat: Date.now(),
        sub: user.authSub,
      },
      config.jwt.secretTfa
    );

    return res.status(200).send({
      success: true,
      message: 'Two step verification has been enabled',
      tfaToken,
    });
  }

  return res.status(200).send({
    success: false,
    message: 'Invalid token provided, Please try again',
  });
});

const verifyOTP = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { token } = req.body;

  if (!token) {
    return res.status(200).send({
      success: false,
      message: 'Token is required for Two step authentication verification',
    });
  }
  const user = await userService.getUserByEmail(email);

  if (!user.tfa_active) {
    return res.status(200).send({
      success: false,
      message: 'Two factor verification is not enabled',
    });
  }

  const verified = speakeasy.totp.verify({
    secret: user.tfa.base32,
    encoding: 'base32',
    token,
  });

  if (verified) {
    const tfaToken = jwt.sign(
      {
        iat: Date.now(),
        sub: user.authSub,
      },
      config.jwt.secretTfa
    );

    return res.status(200).send({
      success: true,
      tfaToken,
    });
  }

  return res.status(200).send({
    success: false,
    message: 'Invalid token provided, Please try again',
  });
});

// const disableUserTfa = catchAsync(async (req, res) => {
//   const { email } = req.auth.cshInfo;
//   const { token } = req.body;

//   if (!token) {
//     return res.status(200).send({
//       success: false,
//       message: 'Token is required for Two step authentication verification',
//     });
//   }
//   const user = await userService.getUserByEmail(email);

//   if (!user.tfa_active) {
//     return res.status(200).send({
//       success: false,
//       message: 'Two factor verification is not enabled',
//     });
//   }

//   const verified = speakeasy.totp.verify({
//     secret: user.tfa.base32,
//     encoding: 'base32',
//     token,
//   });

//   if (verified) {
//     user.tfa_active = false;

//     await user.save();

//     return res.status(200).send({
//       success: true,
//       message: 'Two step verification has been disabled',
//     });
//   }

//   return res.status(200).send({
//     success: false,
//     message: 'Invalid token provided, Please try again',
//   });
// });

const getUserTickets = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);

  return res.status(200).send({
    success: true,
    tickets: user.tickets || [],
  });
});

const createSupportTicket = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);
  const { subject, issueType, description } = req.body;

  const ticket = await ticketService.createTicket({
    subject,
    issueType,
    associatedUser: user._id.toString(),
    lastMessageDate: new Date().toISOString(),
    messages: [
      {
        sender: user.userName,
        senderName: user.nameString,
        date: new Date().toISOString(),
        message: description,
      },
    ],
  });

  const userTickets = user.tickets;

  userTickets.unshift({
    subject,
    issueType,
    ticketNumber: ticket.ticketNumber,
    createdAt: new Date().toISOString(),
    updatedAt: new Date().toISOString(),
    associatedUser: user._id.toString(),
    lastMessageDate: new Date().toISOString(),
    status: 1,
    messages: [
      {
        sender: user.userName,
        senderName: user.nameString,
        date: new Date().toISOString(),
        message: description,
      },
    ],
  });

  user.tickets = userTickets;

  await user.save();

  return res.status(200).send({
    success: true,
    message: `Ticket ${ticket.ticketNumber} has been created`,
  });
});

const viewSupportTicket = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);
  const { ticketNumber } = req.params;

  const ticket = await ticketService.findTicketByNumber(ticketNumber);

  if (!ticket) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  if (ticket.associatedUser !== user._id.toString()) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  const { subject, issueType, messages, status } = ticket;

  return res.status(200).send({
    success: true,
    ticket: {
      ticketNumber,
      subject,
      issueType,
      messages,
      status,
    },
  });
});

const answerSupportTicket = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);
  const { ticketNumber } = req.params;
  const { description } = req.body;

  const ticket = await ticketService.findTicketByNumber(ticketNumber);

  if (!ticket) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  if (ticket.associatedUser !== user._id.toString()) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  const ticketMessages = ticket.messages;
  const userTicket = _.filter(user.tickets, (x) => x.ticketNumber === ticketNumber)[0];
  const userMessages = userTicket.messages;
  const ticketIndex = _.findIndex([...user.tickets], (x) => x.ticketNumber === ticketNumber);

  ticketMessages.push({
    sender: user.userName,
    senderName: user.nameString,
    date: new Date().toISOString(),
    message: description,
  });
  userMessages.push({
    sender: user.userName,
    senderName: user.nameString,
    date: new Date().toISOString(),
    message: description,
  });

  if (ticket.status !== 1) {
    ticket.status = 1;
    userTicket.status = 1;
  }

  ticket.messages = [...ticketMessages];
  ticket.lastMessageDate = new Date().toISOString();
  userTicket.lastMessageDate = new Date().toISOString();
  userTicket.messages = [...userMessages];
  user.tickets[ticketIndex] = { ...userTicket };

  await ticket.save();
  await user.save();

  return res.status(200).send({
    success: true,
    message: 'Your answer has been submitted',
  });
});

const closeSupportTicket = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);
  const { ticketNumber } = req.params;

  const ticket = await ticketService.findTicketByNumber(ticketNumber);

  if (!ticket) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  if (ticket.associatedUser !== user._id.toString()) {
    return res.status(200).send({
      success: false,
      message: 'Ticket not found or you are not authorized to view this ticket',
    });
  }

  const userTicket = _.filter(user.tickets, (x) => x.ticketNumber === ticketNumber)[0];
  const ticketIndex = _.findIndex([...user.tickets], (x) => x.ticketNumber === ticketNumber);

  ticket.status = 3;
  userTicket.status = 3;

  user.tickets[ticketIndex] = { ...userTicket };
  ticket.markModified('status');

  await ticket.save();
  await user.save();

  return res.status(200).send({
    success: true,
    message: `Ticket ${ticketNumber} has been closed`,
  });
});

const getUserBalance = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { balance } = await profileService.getUserBalance(email);

  return res.status(200).send({
    success: true,
    balance,
  });
});

const getUserWithdrawDestinations = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const destinations = await profileService.getUserPaymentDestinations(email);

  return res.status(200).send({
    success: true,
    destinations,
  });
});

const getUserWithdrawBalance = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { balance } = await profileService.getUserWithdrawBalance(email);

  return res.status(200).send({
    success: true,
    balance,
  });
});

const getWithdrawBalanceAndDestinations = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;
  const { balance } = await profileService.getUserWithdrawBalance(email);
  const destinations = await profileService.getUserPaymentDestinations(email);

  return res.status(200).send({
    success: true,
    balance,
    destinations,
  });
});

const createWithdrawRequest = catchAsync(async (req, res) => {
  const { amount, ac } = req.body;

  const { email } = req.auth.cshInfo;
  const user = await userService.getUserByEmail(email);
  const decoded = jwt.verify(ac, config.jwt.secretTfa);

  if (!decoded) {
    throw new ApiError(httpStatus.OK, 'Invalid Payment Information provided');
  }

  const { balance } = await profileService.getUserWithdrawBalance(email);

  if (balance < amount) {
    throw new ApiError(httpStatus.OK, `You don't have enough balance to complete this transaction`);
  }

  const { customerAuthorizations } = user;

  const targetAuthorization = _.filter(customerAuthorizations, (x) => x.authorization.authorization_code === decoded);

  if (!targetAuthorization) {
    throw new ApiError(httpStatus.OK, `You don't have access to submit withdraws to this destination`);
  }

  const withdraw = await transactionService.createWithdrawTransaction({
    amount,
    assocUser: user._id.toString(),
    customerAuthorization: targetAuthorization[0],
  });

  const response = await axios.post(
    'https://api.paystack.co/transferrecipient',
    {
      type: 'authorization',
      name: user.nameString,
      email: user.email,
      authorization_code: targetAuthorization[0].authorization.authorization_code,
    },
    {
      headers: {
        Authorization: `Bearer ${config.paystack_secret}`,
        'Content-Type': 'application/json',
      },
      validateStatus: false,
    }
  );

  if (response.status === 201 && response.data && response.data.status === true) {
    withdraw.transferRecipient = response.data;
    await withdraw.save();

    return res.status(200).send({
      success: true,
      wid: withdraw._id.toString(),
    });
  }

  return res.status(200).send({
    success: false,
    message: response.data.message,
  });
});

const viewWithdrawRequest = catchAsync(async (req, res) => {
  const { wid } = req.params;
  const { email } = req.auth.cshInfo;

  const user = await userService.getUserIdByEmail(email);
  const withdraw = await Withdraw.findById(wid);

  if (!withdraw) {
    throw new ApiError(httpStatus.OK, 'Request not found');
  }

  if (withdraw.assocUser !== user.toString()) {
    throw new ApiError(httpStatus.OK, `you don't have access to view this request`);
  }

  const { amount, customerAuthorization, status, invoiceNumber } = withdraw;

  return res.status(200).send({
    status,
    amount,
    invoiceNumber,
    destination: {
      digits: customerAuthorization.authorization.last4,
      month: customerAuthorization.authorization.exp_month,
      year: customerAuthorization.authorization.exp_year,
      type: customerAuthorization.authorization.card_type,
      brand: customerAuthorization.authorization.brand,
      bank: customerAuthorization.authorization.bank,
      holder: customerAuthorization.authorization.account_name,
    },
  });
});

const confirmWithdrawRequest = catchAsync(async (req, res) => {
  const { wid } = req.params;
  const { email } = req.auth.cshInfo;
  const { token } = req.body;

  const user = await userService.getUserByEmail(email);
  const withdraw = await Withdraw.findById(wid);

  if (!withdraw) {
    throw new ApiError(httpStatus.OK, 'Request not found');
  }

  if (withdraw.assocUser !== user._id.toString()) {
    throw new ApiError(httpStatus.OK, `you don't have access to confirm this request`);
  }
  const verified = speakeasy.totp.verify({
    secret: user.tfa.base32,
    encoding: 'base32',
    token,
  });

  if (!verified) {
    throw new ApiError(httpStatus.OK, `Invalid two factor authentication code`);
  }

  const response = await axios.post(
    'https://api.paystack.co/transfer',
    {
      source: 'balance',
      amount: withdraw.amount.toString(),
      reference: withdraw._id.toString(),
      recipient: withdraw.transferRecipient.data.recipient_code,
      reason: `Withdraw Request ${withdraw.invoiceNumber} from Cashcashe`,
    },
    {
      headers: {
        Authorization: `Bearer ${config.paystack_secret}`,
        'Content-Type': 'application/json',
      },
      validateStatus: false,
    }
  );

  if (response.status !== 200 || !response.data || response.data.status !== true) {
    return res.status(200).send({
      success: false,
      message: response.data.message,
    });
  }

  withdraw.initiateTransfer = response.data;

  await withdraw.save();

  await transactionService.createTransaction({
    type: 2,
    amount: withdraw.amount,
    absAmount: withdraw.amount,
    fees: 0,
    currency: 'NGN',
    reference: withdraw._id.toString(),
    assocUser: user._id.toString(),
    paymentPayload: {},
    paymentVerificationResponse: {},
    transactionInstance: 'IPG',
  });

  return res.status(200).send({
    success: true,
  });
});

module.exports = {
  userDashboardInfo,
  userTransactions,
  userDeposits,
  userWithdraws,
  userGames,
  userDetail,
  updateUserInfo,
  getUserTfaState,
  enableUserTfa,
  verifyUserTfa,
  getUserTickets,
  createSupportTicket,
  viewSupportTicket,
  answerSupportTicket,
  closeSupportTicket,
  getUserBalance,
  verifyOTP,
  getUserWithdrawDestinations,
  getUserWithdrawBalance,
  getWithdrawBalanceAndDestinations,
  createWithdrawRequest,
  viewWithdrawRequest,
  confirmWithdrawRequest,
};
