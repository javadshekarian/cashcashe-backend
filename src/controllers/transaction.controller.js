const httpStatus = require('http-status');
const _ = require('lodash');
const catchAsync = require('../utils/catchAsync');
const {
  createTransaction,
  findTransactionByReference,
  validateTransactionPayload,
  verifyTransactionByReference,
} = require('../services/transaction.service');
const { getUserIdByEmail, getUserByEmail } = require('../services/user.service');

const createPayTransaction = catchAsync(async (req, res) => {
  const { amount } = req.body;

  const _transaction = await createTransaction({
    type: 1,
    amount,
    assocUser: 'test',
  });

  return res.status(httpStatus.OK).send({
    success: true,
    message: `Transaction has been created, it can be paid using it's TID`,
    tid: _transaction._id,
  });
});

const chargeAccount = catchAsync(async (req, res) => {
  const { email } = req.auth.cshInfo;

  const { isValid } = validateTransactionPayload(req.body);
  const uid = await getUserIdByEmail(email);
  const _user = await getUserByEmail(email);

  if (!isValid) {
    return res.status(200).send({
      success: false,
      message: `Transaction verification failed, If you think this is a mistake, please contact support with given information for manual reviewing your transaction`,
      elements: [`Transaction Reference : ${req.body.reference}`, `Transaction ID : ${req.body.trans}`],
    });
  }
  const { reference } = req.body;

  const _duplicate = await findTransactionByReference(reference);

  if (_duplicate) {
    return res.status(200).send({
      success: false,
      message: 'Transaction with the same information already exist. Double spending is not allowed',
    });
  }

  const response = await verifyTransactionByReference(reference);

  if (response.success) {
    const { amount, absAmount, fees, currency, verificationResponse } = response;

    const _transaction = await createTransaction({
      type: 1,
      amount,
      absAmount,
      fees,
      currency,
      reference,
      assocUser: uid,
      event: {
        date: new Date().toLocaleString(),
        info: 'Transaction has been paid by the user',
        status: 10,
      },
      paymentPayload: req.body,
      paymentVerificationResponse: verificationResponse,
    });

    let { customerAuthorizations } = _user;
    if (!customerAuthorizations) customerAuthorizations = [];
    const { data } = verificationResponse;

    customerAuthorizations.push({
      authorization: data.authorization,
      customer: data.customer,
    });

    _user.customerAuthorizations = _.uniqWith(customerAuthorizations, _.isEqual);

    await _user.save();

    return res.status(200).send({
      success: true,
      message: `${amount} ${currency} has been credited to your account, Happy games!`,
      elements: [
        `Transaction Reference : ${req.body.reference}`,
        `Transaction ID : ${req.body.trans}`,
        `Invoice Number : ${_transaction.invoiceNumber}`,
      ],
    });
  }

  const { amount, currency, verificationResponse } = response;

  const _transaction = await createTransaction({
    type: 1,
    amount,
    currency,
    reference,
    assocUser: uid,
    event: {
      date: new Date().toLocaleString(),
      info: 'Transaction payment failed',
      status: 20,
    },
    paymentPayload: req.body,
    paymentVerificationResponse: verificationResponse,
  });

  return res.status(200).send({
    success: false,
    message: `Transaction verification failed, If you think this is a mistake, please contact support with given information for manual reviewing your transaction`,
    elements: [
      `Transaction Reference : ${req.body.reference}`,
      `Transaction ID : ${req.body.trans}`,
      `Invoice Number : ${_transaction.invoiceNumber}`,
    ],
  });
});

module.exports = {
  createPayTransaction,
  chargeAccount,
};
