const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService, contactService, authService } = require('../services');
const GToken = require('../models/gToken.model');

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  res.status(httpStatus.CREATED).send(user);
});

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  res.send(result);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.params.userId, req.body);
  res.send(user);
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

const contact = catchAsync(async (req, res) => {
  const { token } = req.body;
  const duplicateToken = await GToken.find({
    gToken: token,
  });

  if (duplicateToken.length > 0) {
    throw new ApiError(200, 'Recaptcha verification failed, please reload window and try again');
  }
  const recaptchaResponse = await authService.validateHuman(token);

  const gTokenItem = new GToken({
    gToken: token,
    gResponse: recaptchaResponse.data,
  });

  await gTokenItem.save();

  if (!recaptchaResponse.data.success) {
    throw new ApiError(200, 'Recaptcha verification failed, please try again');
  }

  await contactService.createContactMessage(req.body);

  return res.status(200).send({
    success: true,
    message: `Thank you! We've received your message, we'll be in touch with you soon.`,
  });
});

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  contact,
};
