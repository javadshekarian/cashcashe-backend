const multer = require('multer');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, '/tmp');
  },
  filename(req, file, cb) {
    const ext = file.originalname.substr(file.originalname.lastIndexOf('.'));

    cb(null, `${file.fieldname}_${(Math.random() + 1).toString(36).substring(7)}_${Date.now()}${ext}`);
  },
});

const store = multer({ storage });

module.exports = store;
