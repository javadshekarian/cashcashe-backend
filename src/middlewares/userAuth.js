const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const { User } = require('../models/index');
const { userService } = require('../services');
const ApiError = require('../utils/ApiError');
const config = require('../config/config');

const userCheck = async (req, resolve, reject, props = {}) => {
  const { cshInfo, sub } = req.auth;
  const tfaToken = req.headers.tfa;

  if (!cshInfo) {
    return reject(new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate'));
  }

  if (!tfaToken && !props.noTFA) {
    return reject(new ApiError(httpStatus.UNAUTHORIZED, 'Two factor authentication required'));
  }

  const _currentUser = await User.findOne({
    userName: cshInfo.nickname,
  });

  if (_currentUser) {
    const userState = await userService.userStatusCheck(_currentUser);
    if (!userState.success) {
      return reject(new ApiError(httpStatus.BAD_REQUEST, userState.message));
    }
  } else {
    const _newUser = new User({
      name: cshInfo.given_name,
      lastName: cshInfo.family_name,
      userName: cshInfo.nickname,
      nameString: cshInfo.name || '',
      profilePicture: {
        url: cshInfo.picture,
        changeLog: [{ date: new Date().toISOString(), url: cshInfo.picture }],
      },
      authSub: sub,
      email: cshInfo.email,
      role: 'user',
      isEmailVerified: cshInfo.email_verified,
    });
    await _newUser.save();
  }
  let token;
  try {
    token = jwt.verify(tfaToken, config.jwt.secretTfa);

    if (!token && !props.noTFA) {
      reject(new ApiError(httpStatus.UNAUTHORIZED, 'INV_TFA'));
    }
  } catch (error) {
    if (!props.noTFA) {
      reject(new ApiError(httpStatus.UNAUTHORIZED, 'INV_TFA'));
    }
  }
  resolve();
};

const userAuth = (props) => async (req, res, next) => {
  return new Promise((resolve, reject) => {
    userCheck(req, resolve, reject, props);
  })
    .then(() => next())
    .catch((err) => next(err));
};

module.exports = userAuth;
