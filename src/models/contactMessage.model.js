const mongoose = require('mongoose');
const { paginate, toJSON } = require('./plugins');

const contactMessageSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

contactMessageSchema.plugin(paginate);
contactMessageSchema.plugin(toJSON);

const ContactMessage = mongoose.model('Contact_Message', contactMessageSchema);

module.exports = ContactMessage;
