const mongoose = require('mongoose');

const gTokenSchema = mongoose.Schema(
  {
    gToken: {
      type: String,
      required: true,
    },
    gResponse: {
      type: Object,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

/**
 * @typedef Popup
 */
const GToken = mongoose.model('GToken', gTokenSchema);

module.exports = GToken;
