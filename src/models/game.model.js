const mongoose = require('mongoose');
const mongooseSerial = require('mongoose-serial');
const config = require('../config/config');
const { toJSON, paginate } = require('./plugins');

// instantiate a mongoose schema
const gameSchema = new mongoose.Schema(
  {
    instance: {
      type: String,
      enum: config.games.gameIDs,
      required: true,
    },
    invest_amount: {
      type: Number,
      default: 100,
      required: true,
    },
    return_amount: {
      type: Number,
    },
    invest_currency: {
      type: String,
      default: 'NGN',
      required: true,
    },
    player: {
      type: String,
      required: true,
    },
    playedByAgent: {
      type: Boolean,
      default: false,
      private: true,
    },
    playerAgent: {
      type: String,
      private: true,
    },
    gameRule: {
      type: Object,
      required: true,
      private: true,
    },
    isFinished: {
      type: Boolean,
      default: false,
      required: true,
    },
    result: {
      type: Number,
    },
    gameCache: {
      type: Array,
      required: true,
      private: true,
    },
    startedAt: {
      type: Date,
      default: Date.now(),
    },
    endedAt: {
      type: Date,
    },
    userPayload: {
      type: Object,
    },
    calculatedPayload: {
      type: Object,
    },
    gameSerial: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

gameSchema.plugin(mongooseSerial, {
  field: 'gameSerial',
  prefix: 'CGS',
  separator: '-',
  initCounter: 12345,
  digits: 5,
});

gameSchema.plugin(paginate);

gameSchema.plugin(toJSON);
// create a model from schema and export it
module.exports = mongoose.model('Game', gameSchema);
