const mongoose = require('mongoose');
const mongooseSerial = require('mongoose-serial');
const { toJSON } = require('./plugins');

// instantiate a mongoose schema
const RuleSchema = new mongoose.Schema(
  {
    targetGame: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    rulePrizes: [
      {
        prizeLabel: {
          type: String,
          required: [true, 'Prize Label is Required'],
        },
        prizeValue: {
          type: Number,
          required: [true, 'Prize Value is Required'],
        },
        prizeLimit: {
          type: Number,
          required: [true, 'Prize Limit is required'],
        },
      },
    ],
    ruleNumber: {
      type: String,
      required: false,
    },
    createdBy: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

RuleSchema.plugin(mongooseSerial, {
  field: 'ruleNumber',
  prefix: 'RLE',
  separator: '-',
  initCounter: 12345,
  digits: 5,
});

RuleSchema.plugin(toJSON);
// create a model from schema and export it
module.exports = mongoose.model('Game_Rule', RuleSchema);
