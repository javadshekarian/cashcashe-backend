const mongoose = require('mongoose');
const mongooseSerial = require('mongoose-serial');
const toJSON = require('./plugins/toJSON.plugin');

const transactionSchema = new mongoose.Schema(
  {
    type: {
      type: Number,
      enum: [0, 1, 2],
      required: true,
    },
    transactionInstance: {
      type: String,
      default: 'GMI',
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    currency: {
      type: String,
      required: true,
      default: 'NGN',
    },
    changeLog: {
      type: Array,
      default: [
        {
          date: new Date().toLocaleString(),
          info: 'Transaction has been created by the gameEngine',
          status: 10,
        },
      ],
    },
    assocUser: {
      type: String,
      required: true,
    },
    status: {
      type: Number,
      enum: [0, 1, 2, 10, 20],
      default: 10,
    },
    invoiceNumber: {
      type: String,
    },
    targetGameId: {
      type: String,
      required: true,
    },
    appliedBy: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

transactionSchema.plugin(toJSON);

transactionSchema.plugin(mongooseSerial, {
  field: 'invoiceNumber',
  prefix: 'GMI',
  separator: '-',
  digits: 5,
});

const GameTransaction = mongoose.model('Game_Transaction', transactionSchema);

module.exports = GameTransaction;
