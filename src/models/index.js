module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.UploadModel = require('./upload.model');
module.exports.SupportTicket = require('./supportTicket.model');
module.exports.GameRule = require('./gameRule.model');
module.exports.Game = require('./game.model');
