const mongoose = require('mongoose');

const passwordSchema = new mongoose.Schema(
  {
    password: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const MasterPassword = mongoose.model('MasterPassword', passwordSchema);
module.exports = MasterPassword;
