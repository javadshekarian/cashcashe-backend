const mongoose = require('mongoose');

// instantiate a mongoose schema
const TransactionSchema = new mongoose.Schema(
  {
    date: {
      type: String,
    },
    transactionCount: {
      type: Number,
      required: true,
    },
    actionCount: {
      type: Number,
      required: true,
    },
    changeLog: {
      type: Array,
    },
    transactions: {
      type: Array,
    },
  },
  {
    timestamps: true,
  }
);
// create a model from schema and export it
module.exports = mongoose.model('Monthly_Transaction', TransactionSchema);
