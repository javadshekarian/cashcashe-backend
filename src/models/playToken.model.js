const mongoose = require('mongoose');

const tokenSchema = new mongoose.Schema(
  {
    targetGame: {
      type: String,
      required: true,
    },
    player: {
      type: String,
      required: true,
    },
    targetUser: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const PlayToken = mongoose.model('PlayToken', tokenSchema);

module.exports = PlayToken;
