const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const settingSchema = new mongoose.Schema(
  {
    maintenanceMode: {
      type: Boolean,
      required: false,
    },
    siteLogo: {
      type: String,
      required: false,
    },
    siteName: {
      type: String,
      required: false,
    },
    siteDescription: {
      type: String,
      required: false,
    },
    isActive: {
      type: Boolean,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

settingSchema.plugin(toJSON);
const Setting = mongoose.model('Setting', settingSchema);

module.exports = Setting;
