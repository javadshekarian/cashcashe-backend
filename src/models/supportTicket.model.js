const mongoose = require('mongoose');
const mongooseSerial = require('mongoose-serial');
const { TICKET_ISSUE_TYPES } = require('../validations/profile.validation');
const { paginate, toJSON } = require('./plugins');

const ticketSchema = new mongoose.Schema(
  {
    ticketNumber: {
      type: String,
    },
    lastMessageDate: {
      type: Date,
      required: true,
    },
    status: {
      type: Number,
      required: true,
      enum: [1, 2, 3],
      default: 1,
    },
    messages: [
      {
        sender: {
          type: String,
          required: true,
        },
        senderName: {
          type: String,
          required: true,
        },
        date: {
          type: String,
          required: true,
        },
        message: {
          type: String,
          required: true,
        },
        isAdmin: {
          type: Boolean,
          required: true,
          default: false,
        },
      },
    ],
    issueType: {
      type: String,
      required: true,
      enum: [...TICKET_ISSUE_TYPES.map((item) => item.value)],
    },
    subject: {
      type: String,
      required: true,
    },
    associatedUser: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
ticketSchema.plugin(mongooseSerial, {
  field: 'ticketNumber',
  prefix: 'ZSP',
  separator: '-',
  initCounter: 12345,
  digits: 5,
});
ticketSchema.plugin(paginate);
ticketSchema.plugin(toJSON);
const SupportTicket = mongoose.model('Support_Ticket', ticketSchema);

module.exports = SupportTicket;
