const mongoose = require('mongoose');
const mongooseSerial = require('mongoose-serial');
const { paginate, toJSON } = require('./plugins');

const transactionSchema = new mongoose.Schema(
  {
    type: {
      type: Number,
      enum: [0, 1, 2],
      required: true,
    },
    transactionInstance: {
      type: String,
      default: 'IPG',
    },
    amount: {
      type: Number,
      required: true,
    },
    absAmount: {
      type: Number,
      required: false,
    },
    fees: {
      type: Number,
      required: false,
    },
    currency: {
      type: String,
      required: true,
    },
    assocUser: {
      type: String,
      required: true,
    },
    changeLog: {
      type: Array,
      required: true,
    },
    reference: {
      type: String,
      required: true,
    },
    status: {
      type: Number,
      enum: [0, 1, 2, 10, 20],
    },
    invoiceNumber: {
      type: String,
    },
    paymentPayload: {
      type: Object,
      required: true,
      private: true,
    },
    paymentVerificationResponse: {
      type: Object,
      required: false,
      private: true,
    },
  },
  {
    timestamps: true,
  }
);

transactionSchema.plugin(mongooseSerial, {
  field: 'invoiceNumber',
  prefix: 'INV',
  separator: '-',
  digits: 5,
});

transactionSchema.plugin(paginate);
transactionSchema.plugin(toJSON);

const Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;
