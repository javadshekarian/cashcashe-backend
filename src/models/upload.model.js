const mongoose = require('mongoose');

const uploadSchema = new mongoose.Schema(
  {
    fileName: {
      type: String,
      required: true,
    },
    contentType: {
      type: String,
      required: true,
    },
    imageBase64: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const UploadModel = mongoose.model('Upload', uploadSchema);

module.exports = UploadModel;
