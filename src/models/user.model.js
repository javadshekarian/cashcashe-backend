const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');
const { roles } = require('../config/roles');
const config = require('../config/config');

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
    },
    lastName: {
      type: String,
      trim: true,
    },
    userName: {
      type: String,
      required: true,
      trim: true,
    },
    nameString: {
      type: String,
      required: false,
      trim: true,
    },
    profilePicture: {
      type: Object,
      required: false,
    },
    authSub: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    password: {
      type: String,
      required: false,
      trim: true,
      minlength: 8,
      validate(value) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error('Password must contain at least one letter and one number');
        }
      },
      private: true, // used by the toJSON plugin
    },
    role: {
      type: String,
      enum: roles,
      default: 'user',
    },
    status: {
      type: Number,
      default: 0,
    },
    isEmailVerified: {
      type: Boolean,
      default: false,
    },
    games: {
      type: Array,
      required: false,
    },

    gameCache: [
      {
        instance: {
          type: String,
          required: true,
          enum: config.games.gameIDs,
        },
        date: {
          type: Date,
          required: true,
        },
      },
    ],
    transactions: {
      type: Array,
      required: false,
    },
    tickets: {
      type: Array,
      required: false,
    },
    tfa: {
      type: Object,
      required: false,
    },
    tfa_active: {
      type: Boolean,
      default: false,
    },
    phone: {
      type: String,
    },
    country: {
      type: String,
    },
    address: {
      type: String,
    },
    about: {
      type: String,
    },
    receivedPrizes: {
      type: Array,
      private: true,
    },
    prizeCache: {
      type: Array,
      private: true,
    },
    agents: {
      type: Array,
      required: false,
      default: [],
    },
    customerAuthorizations: {
      type: Array,
      required: false,
      default: [],
    },
    hasInfo: {
      type: Boolean,
      required: false,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.model('User', userSchema);

module.exports = User;
