const mongoose = require('mongoose');
const mongooseSerial = require('mongoose-serial');
const { paginate, toJSON } = require('./plugins');

const withdrawSchema = new mongoose.Schema(
  {
    assocUser: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    customerAuthorization: {
      type: Object,
      required: true,
    },
    transferRecipient: {
      type: Object,
    },
    initiateTransfer: {
      type: Object,
    },
    verifyTransfer: {
      type: Object,
    },
    status: {
      type: Number,
      required: true,
      default: 1,
    },
    invoiceNumber: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

withdrawSchema.plugin(mongooseSerial, {
  field: 'invoiceNumber',
  prefix: 'WTH',
  separator: '-',
  digits: 5,
});

withdrawSchema.plugin(paginate);
withdrawSchema.plugin(toJSON);

const Withdraw = mongoose.model('Withdraw', withdrawSchema);

module.exports = Withdraw;
