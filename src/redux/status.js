const { createStore } = require('redux');

/* eslint-disable default-param-last */
const config = function (
  state = {
    threadOnline: false,
    threadHasTasks: 0,
  },
  action
) {
  switch (action.type) {
    case 'THREAD_ONLINE':
      return {
        ...state,
        threadOnline: true,
      };
    case 'THREAD_OFFLINE':
      return {
        ...state,
        threadOnline: false,
      };
    case 'LOAD_THREAD':
      return {
        ...state,
        threadHasTasks: state.threadHasTasks + 1,
      };
    case 'OFFLOAD_THREAD':
      if (state.threadHasTasks - 1 >= 0) {
        return {
          ...state,
          threadHasTasks: state.threadHasTasks - 1,
        };
      }

      return {
        ...state,
        threadHasTasks: 0,
      };

    default:
      return state;
  }
};

const store = createStore(config);

module.exports = store;
