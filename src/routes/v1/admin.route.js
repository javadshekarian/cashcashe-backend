const express = require('express');
const { adminController } = require('../../controllers');
const { adminValidation } = require('../../validations');
const validate = require('../../middlewares/validate');
const checkJwt = require('../../config/authz');
const userAuth = require('../../middlewares/userAuth');

const router = express.Router();

router.route('/dashboard').get(adminController.getDashboardData);

router.route('/transactions').post(validate(adminValidation.queryTransactions), adminController.queryTransactions);

router.route('/contact-message').post(validate(adminValidation.queryTransactions), adminController.queryContactMessages);

// router.route('/support-tickets/new').post(validate(adminValidation.createTicket), adminController.createSupportTicket);

router.route('/support-tickets').post(validate(adminValidation.queryTransactions), adminController.querySupportTickets);

router
  .route('/support-tickets/:ticketNumber/answer')
  .post([checkJwt, userAuth(), validate(adminValidation.answerTicket)], adminController.answerTicket);

router
  .route('/support-tickets/:ticketNumber/close')
  .get([checkJwt, userAuth(), validate(adminValidation.closeTicket)], adminController.closeTicket);

router.route('/users/:userId/unban').get(validate(adminValidation.userBan), adminController.unbanUser);

router.route('/users/:userId/permanently-ban').get(validate(adminValidation.userBan), adminController.permanentlyBan);

router.route('/users/:userId/temporary-ban').get(validate(adminValidation.userBan), adminController.tempBanUser);

router.route('/users/:userId/debit').post(validate(adminValidation.debitCreditUser), adminController.debitUser);

router.route('/users/:userId/credit').post(validate(adminValidation.debitCreditUser), adminController.creditUser);

router
  .route('/users/:userId')
  .post(validate(adminValidation.updateUser), adminController.updateUser)
  .get(validate(adminValidation.getUserInfo), adminController.getUserInfo);

router.route('/users').post(validate(adminValidation.queryTransactions), adminController.queryUsers);

router.route('/agents').post(validate(adminValidation.queryTransactions), adminController.queryAgents);

router.route('/games').post(validate(adminValidation.queryTransactions), adminController.queryGames);

router.route('/master-password').post(validate(adminValidation.setMasterPassowrd), adminController.setMasterPassowrd);

router
  .route('/settings')
  .get(adminController.getCurrentSetting)
  .post(validate(adminValidation.createSetting), adminController.createSetting);

module.exports = router;
