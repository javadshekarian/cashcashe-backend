const express = require('express');
const validate = require('../../middlewares/validate');
const { agentValidation } = require('../../validations');
const { agentController } = require('../../controllers');
const userAuth = require('../../middlewares/userAuth');
const checkJwt = require('../../config/authz');

const router = express.Router();

router.route('/pre-check').post(validate(agentValidation.creditUser), agentController.preCheckCredit);

router.route('/credit-user').post([checkJwt, userAuth()], validate(agentValidation.creditUser), agentController.creditUser);

router
  .route('/transactions')
  .post([checkJwt, userAuth()], validate(agentValidation.queryTransactions), agentController.queryTransactions);

router
  .route('/get-play-ticket')
  .post([checkJwt, userAuth()], validate(agentValidation.getPlayToken), agentController.getPlayToken);

module.exports = router;
