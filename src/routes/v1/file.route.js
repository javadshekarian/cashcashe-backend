const express = require('express');
const store = require('../../middlewares/multer');
const validate = require('../../middlewares/validate');
const { fileValidation } = require('../../validations');
const { fileController } = require('../../controllers');

const router = express.Router();

router.route('/').post(store.single('file'), fileController.uploadFile);

router.route('/view/:fileId/:fileName').get(validate(fileValidation.viewFile), fileController.viewFile);

module.exports = router;
