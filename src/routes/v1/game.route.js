const express = require('express');
const checkJwt = require('../../config/authz');
const validate = require('../../middlewares/validate');
const { gameValidation } = require('../../validations');
const { gameController } = require('../../controllers');
const userAuth = require('../../middlewares/userAuth');

const router = express.Router();

router.post('/keno', [checkJwt, userAuth()], validate(gameValidation.playKeno), gameController.playKeno);

router.get('/bingo', [checkJwt, userAuth()], gameController.playBingo);

router.get('/bull-and-bear', [checkJwt, userAuth()], gameController.playBullBear);

router.post(
  '/european-roulette',
  [checkJwt, userAuth()],
  validate(gameValidation.playRoulette),
  gameController.playRoulette
);

router.get('/wheel-of-fortune', [checkJwt, userAuth()], gameController.playWheel);

router.get('/slot', [checkJwt, userAuth()], gameController.playSlot);

router.get('/:id', [checkJwt, userAuth()], validate(gameValidation.getGameStatus), gameController.getGameStatus);

router.get('/can-user-play', [checkJwt, userAuth()], gameController.canUserPlay);

module.exports = router;
