const express = require('express');
const validate = require('../../middlewares/validate');
const { gameRuleValidation } = require('../../validations');
const { gameRuleController } = require('../../controllers');

const router = express.Router();

router.get('/:password', validate(gameRuleValidation.getRules), gameRuleController.getRules);

router.post('/', validate(gameRuleValidation.setRules), gameRuleController.setRules);

router.get('/wheel-of-fortune-base', gameRuleController.getWheelOfFortuneBase);

module.exports = router;
