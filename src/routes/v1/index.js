const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const profileRoute = require('./profile.route');
const fileRoute = require('./file.route');
const transactionRoute = require('./transaction.route');
const gameRuleRoute = require('./gameRule.route');
const gameRoute = require('./game.route');
const adminRoute = require('./admin.route');
const agentRoute = require('./agent.route');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/profile',
    route: profileRoute,
  },
  {
    path: '/file',
    route: fileRoute,
  },
  {
    path: '/transaction',
    route: transactionRoute,
  },
  {
    path: '/game-rules',
    route: gameRuleRoute,
  },
  {
    path: '/game',
    route: gameRoute,
  },
  {
    path: '/admin',
    route: adminRoute,
  },
  {
    path: '/agent',
    route: agentRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});
module.exports = router;
