const express = require('express');
const checkJwt = require('../../config/authz');
const validate = require('../../middlewares/validate');
const { profileValidation } = require('../../validations');
const { profileController } = require('../../controllers');
const userAuth = require('../../middlewares/userAuth');

const router = express.Router();

router
  .route('/')
  .get([checkJwt, userAuth()], profileController.userDashboardInfo)
  .post(
    [
      checkJwt,
      userAuth({
        noTFA: true,
      }),
      validate(profileValidation.updateProfile),
    ],
    profileController.updateUserInfo
  );

router.route('/transactions').get([checkJwt, userAuth()], profileController.userTransactions);

router.route('/deposits').get([checkJwt, userAuth()], profileController.userDeposits);

router.route('/withdraws').get([checkJwt, userAuth()], profileController.userWithdraws);

router.route('/games').get([checkJwt, userAuth()], profileController.userGames);

router.route('/info').get(
  [
    checkJwt,
    userAuth({
      noTFA: true,
    }),
  ],
  profileController.userDetail
);

router.route('/balance').get([checkJwt, userAuth()], profileController.getUserBalance);

router.route('/security/tfa_state').get([checkJwt, userAuth()], profileController.getUserTfaState);

router.route('/security/enable_tfa').get(
  [
    checkJwt,
    userAuth({
      noTFA: true,
    }),
  ],
  profileController.enableUserTfa
);

router.route('/security/verify_tfa').post(
  [
    checkJwt,
    userAuth({
      noTFA: true,
    }),
    validate(profileValidation.otp),
  ],
  profileController.verifyUserTfa
);
router.route('/security/verify_otp').post(
  [
    checkJwt,
    userAuth({
      noTFA: true,
    }),
    validate(profileValidation.otp),
  ],
  profileController.verifyOTP
);

// router.route('/security/disable_tfa').post([checkJwt, userAuth()], profileController.disableUserTfa);

router.route('/tickets').get([checkJwt, userAuth()], profileController.getUserTickets);

router
  .route('/tickets/create')
  .post([checkJwt, userAuth(), validate(profileValidation.createTicket)], profileController.createSupportTicket);

router
  .route('/tickets/view/:ticketNumber')
  .get([checkJwt, userAuth(), validate(profileValidation.viewTicket)], profileController.viewSupportTicket);

router
  .route('/tickets/answer/:ticketNumber')
  .post([checkJwt, userAuth(), validate(profileValidation.answerTicket)], profileController.answerSupportTicket);

router
  .route('/tickets/close/:ticketNumber')
  .get([checkJwt, userAuth(), validate(profileValidation.closeTicket)], profileController.closeSupportTicket);

router.route('/withdraw-destinations').get([checkJwt, userAuth()], profileController.getUserWithdrawDestinations);

router.route('/withdraw-balance').get([checkJwt, userAuth()], profileController.getUserWithdrawBalance);

router
  .route('/withdraw-balance-destinations')
  .get([checkJwt, userAuth()], profileController.getWithdrawBalanceAndDestinations);

router
  .route('/request-withdraw')
  .post([checkJwt, userAuth(), validate(profileValidation.requestWithdraw)], profileController.createWithdrawRequest);

router
  .route('/view-withdraw-request/:wid')
  .get([checkJwt, userAuth(), validate(profileValidation.viewWithdrawRequest)], profileController.viewWithdrawRequest);

router
  .route('/confirm-withdraw-request/:wid')
  .post(
    [checkJwt, userAuth(), validate(profileValidation.confirmWithdrawRequest)],
    profileController.confirmWithdrawRequest
  );

module.exports = router;
