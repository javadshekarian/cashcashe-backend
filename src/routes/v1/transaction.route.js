const express = require('express');
const validate = require('../../middlewares/validate');
const userAuth = require('../../middlewares/userAuth');
const checkJwt = require('../../config/authz');
const { transactionValidation } = require('../../validations');
const { transactionController } = require('../../controllers');

const router = express.Router();

router.post(
  '/topup-account',
  [checkJwt, userAuth()],
  validate(transactionValidation.topupAccount),
  transactionController.chargeAccount
);

module.exports = router;
