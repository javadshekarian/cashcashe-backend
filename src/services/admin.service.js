/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
const moment = require('moment');
const Game = require('../models/game.model');
const Transaction = require('../models/transaction.model');
const User = require('../models/user.model');
const SupportTicket = require('../models/supportTicket.model');
const Setting = require('../models/settings.model');
const GameTransaction = require('../models/gameTransaction.model');
const userService = require('./user.service');

const getTodayGames = async () => {
  const _games = await Game.find({
    createdAt: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().add(1, 'day').startOf('day').toDate(),
    },
  });

  if (_games.length < 1) {
    return {
      total_games: 0,
      total_won: 0,
      total_lost: 0,
    };
  }

  const won = _games.filter((x) => x.result === 2);
  const lost = _games.filter((x) => x.result === 1);

  return {
    total_games: _games.length,
    total_won: won.length,
    total_lost: lost.length,
  };
};

const getTodayTransactions = async () => {
  const _ipgTransactions = await Transaction.find({
    createdAt: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().add(1, 'day').startOf('day').toDate(),
    },
    transactionInstance: 'IPG',
  });

  const _gameTransactions = await GameTransaction.find({
    createdAt: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().add(1, 'day').startOf('day').toDate(),
    },
  });

  const withdraw = _ipgTransactions.filter((x) => x.type === 2);
  const deposit = _ipgTransactions.filter((x) => x.type === 1);

  const fees = _gameTransactions.filter((x) => x.type === 2);
  const awards = _gameTransactions.filter((x) => x.type === 1);

  return {
    turnover: _ipgTransactions.reduce((acc, i) => acc + i.amount, 0),
    deposit: deposit.reduce((acc, i) => acc + i.amount, 0),
    withdraw: withdraw.reduce((acc, i) => acc + i.amount, 0),
    fees: fees.reduce((acc, i) => acc + i.amount, 0),
    awards: awards.reduce((acc, i) => acc + i.amount, 0),
  };
};

const queryTransactions = async (filter, options) => {
  const _queried = await Transaction.paginate(filter, options);
  return _queried;
};

const queryUsers = async (filter, options) => {
  const _queried = await User.paginate(filter, options);
  return _queried;
};

const querySupportTickets = async (filter, options) => {
  const _queried = await SupportTicket.paginate(filter, options);
  return _queried;
};

const queryGames = async (filter, options) => {
  const _queried = await Game.paginate(filter, options);
  return _queried;
};

const deactivateCurrentSetting = async () => {
  const _current = await Setting.findOne({
    isActive: true,
  });

  if (!_current) {
    return;
  }

  _current.isActive = false;
  _current.markModified('isActive');
  await _current.save();
};

const createSetting = async (body) => {
  await deactivateCurrentSetting();
  const _setting = new Setting({
    ...body,
    isActive: true,
  });
  await _setting.save();
};

const getSetting = async () => {
  const _setting = await Setting.findOne({
    isActive: true,
  });

  return _setting;
};

const getAgents = async (agents) => {
  const res = [];
  for (let i = 0; i < agents.length; i++) {
    const _user = await userService.getUserById(agents[i]);
    res.push({
      assocUserName: _user.userName,
      email: _user.email,
    });
  }

  return res;
};

module.exports = {
  getTodayGames,
  getTodayTransactions,
  queryTransactions,
  queryUsers,
  querySupportTickets,
  createSetting,
  getSetting,
  queryGames,
  getAgents,
};
