const ContactMessage = require('../models/contactMessage.model');

const createContactMessage = async (messageBody) => {
  return ContactMessage.create(messageBody);
};

const queryContactMessages = async (filter, options) => {
  const messages = await ContactMessage.paginate(filter, options);
  return messages;
};

module.exports = {
  createContactMessage,
  queryContactMessages,
};
