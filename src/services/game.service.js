const _ = require('lodash');
const { Game, GameRule, User } = require('../models');
const ApiError = require('../utils/ApiError');
const config = require('../config/config');
const profileService = require('./profile.service');
const { transactionService } = require('.');
const {
  winRoulette,
  loseRoulette,
  winBingo,
  loseBingo,
  winKeno,
  loseKeno,
  loseBullBear,
  loseWheel,
  winBullBear,
  winWheel,
  loseSlot,
  winSlot,
} = require('./gameResults.service');

const createGame = async (gameBody, asPlayer) => {
  const { instance, player } = gameBody;

  const gameRule = await GameRule.findOne({
    targetGame: instance,
    isActive: true,
  });

  const user = await User.findById(player);

  if (!user) {
    throw new ApiError(
      200,
      'Could not load player profile, if this message persists contact support team for furthur information'
    );
  }

  if (!gameRule) {
    throw new ApiError(
      200,
      'Could not load profile information of the player, if this message persists contact support team for furthur information'
    );
  }

  const { balance } = await profileService.getUserBalance(user.email);

  if (balance < config.games.minimumGameBalance) {
    throw new ApiError(200, `You don't have sufficient balance to play this game, topup your account and try again`);
  }

  if (!user.gameCache) {
    user.gameCache = [];
  }

  user.gameCache.push({
    instance,
    date: Date.now(),
  });

  user.markModified('gameCache');

  const currentPlays = await Game.find({
    instance,
    isFinished: false,
    player,
  });

  if (currentPlays.length > 0) {
    throw new ApiError(
      200,
      'You are currently playing another game, please finish it before starting a new one, if this message persists contact support team for furthur information'
    );
  }

  const game = new Game({
    ...gameBody,
    gameRule: gameRule.toJSON(),
    playedByAgent: !player === asPlayer,
    playerAgent: player === asPlayer ? '' : asPlayer,
    gameCache: user.gameCache,
    invest_amount: config.games.gamePlayCost,
  });

  await game.save();

  if (!user.games) {
    user.games = [];
  }

  user.games.push(game.toJSON());

  user.markModified('games');
  await user.save();

  await transactionService.createGameTransaction({
    type: 2,
    amount: config.games.gamePlayCost,
    assocUser: player,
    targetGameId: game._id,
    appliedBy: asPlayer || player,
  });

  return game;
};

const getGameById = async (id) => {
  const game = await Game.findById(id);

  return game;
};

const generateLosePayload = ({ instance, payload, prizeList }) => {
  let result = [];
  switch (instance) {
    case 'KN':
      result = loseKeno(payload);
      break;
    case 'BBI':
      result = loseBingo();
      break;
    case 'BBE':
      result = loseBullBear(prizeList);
      break;
    case 'ER':
      result = loseRoulette(payload);
      break;
    case 'WF':
      result = loseWheel();
      break;
    case 'SL':
      result = loseSlot();
      break;
    default:
      break;
  }

  return result;
};

const generateWinPayload = ({ instance, payload, prize, prizeList }) => {
  let result = [];
  switch (instance) {
    case 'KN':
      result = winKeno(payload);
      break;
    case 'BBI':
      result = winBingo();
      break;
    case 'BBE':
      result = winBullBear(prize, prizeList);
      break;
    case 'ER':
      result = winRoulette(payload);
      break;
    case 'WF':
      result = winWheel(prize, prizeList);
      break;
    case 'SL':
      result = winSlot();
      break;
    default:
      break;
  }

  return result;
};

const calulcateGameResult = async (item) => {
  const { player, gameRule, gameCache, instance, userPayload, asPlayer } = item;

  const user = await User.findById(player);

  if (!user.receivedPrizes) user.receivedPrizes = [];
  if (!user.prizeCache) user.prizeCache = [];

  const limit = _.filter(gameCache, (x) => x.instance === instance).length;

  const prizes = _.filter(
    gameRule.rulePrizes,
    (x) => x.prizeLimit <= limit && _.filter(user.prizeCache, (y) => y._id.toString() === x._id.toString()).length < 1
  );

  if (prizes.length < 1) {
    const losePayload = generateLosePayload({ instance, payload: userPayload, prizeList: gameRule.rulePrizes });

    await Game.findByIdAndUpdate(item._id, {
      isFinished: true,
      calculatedPayload: losePayload,
      endedAt: Date.now(),
      result: 1,
      return_amount: 0,
    });
  } else {
    const winAmount = prizes.reduce(function (total, prize) {
      return total + prize.prizeValue;
    }, 0);

    const maxLimit = gameRule.rulePrizes.reduce(function (max, prize) {
      return max > prize.prizeLimit ? max : prize.prizeLimit;
    }, 0);

    prizes.map((receivedItem) => {
      user.receivedPrizes.push({ ...receivedItem, date: Date.now() });
      user.prizeCache.push(receivedItem);
      return null;
    });

    await transactionService.createGameTransaction({
      type: 1,
      amount: winAmount,
      assocUser: player,
      targetGameId: item._id,
      appliedBy: asPlayer || player,
    });

    if (limit >= maxLimit) {
      const otherCache = _.filter(user.gameCache, (x) => x.instance !== instance);
      const remainingCache = _.drop(
        _.filter(user.gameCache, (x) => x.instance === instance),
        maxLimit
      );

      user.prizeCache = _.filter(
        user.prizeCache,
        (x) => _.filter(gameRule.rulePrizes, (y) => x._id.toString() === y._id.toString()).length < 1
      );

      user.gameCache = _.union(otherCache, remainingCache);
    }
    const winPayload = generateWinPayload({ instance, payload: userPayload, prize: prizes, prizeList: gameRule.rulePrizes });
    await Game.findByIdAndUpdate(item._id, {
      isFinished: true,
      calculatedPayload: winPayload,
      endedAt: Date.now(),
      result: 2,
      return_amount: winAmount,
    });
  }

  const gameIndex = _.findIndex(user.games, (x) => x.id === item._id.toString());
  const currentGameState = await Game.findById(item._id);
  user.games[gameIndex] = currentGameState.toJSON();
  await User.findByIdAndUpdate(player, {
    games: _.cloneDeep(user.games),
    gameCache: _.cloneDeep(user.gameCache),
    prizeCache: _.cloneDeep(user.prizeCache),
    receivedPrizes: _.cloneDeep(user.receivedPrizes),
  });
};

const handleResults = async (items) => {
  if (items.length > 0) {
    // eslint-disable-next-line no-restricted-syntax, guard-for-in
    for (const item of items) {
      // eslint-disable-next-line no-await-in-loop
      await calulcateGameResult(item);
    }
  }

  return true;
};

const handleUnfinishedGames = async () => {
  const _games = await Game.find({
    isFinished: false,
  });

  await handleResults(_games);
};

module.exports = {
  createGame,
  handleResults,
  getGameById,
  handleUnfinishedGames,
};
