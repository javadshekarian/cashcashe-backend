const _ = require('lodash');
const { roulette, bingoPicks, kenoPicks, bullbearPicks, slotPicks } = require('../utils/gamePicks');

function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const loseRoulette = (payload) => {
  let patternBreaker;

  if (roulette.mods.indexOf(payload) > -1) {
    patternBreaker = roulette[payload];
  } else {
    patternBreaker = [payload];
  }

  const generated = _.pullAll(roulette.numbers, patternBreaker)
    .sort(() => 0.5 - Math.random())
    .slice(0, 1)[0];
  return generated;
};

const winRoulette = (payload) => {
  let possibles;
  if (roulette.mods.indexOf(payload) > -1) {
    possibles = roulette[payload];
    const result = possibles.sort(() => 0.5 - Math.random()).slice(0, 1)[0];
    return result;
  }
  return payload;
};

const winBingo = () => {
  const rows = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < 5; i++) {
    let current = [];
    if (rows.length > 0) {
      current = _.flattenDeep(rows);
    }

    rows[i] = _.filter(_.cloneDeep(bingoPicks), (x) => current.indexOf(x) < 0)
      .sort(() => 0.5 - Math.random())
      .slice(0, 5);
  }

  rows[2][2] = 'STAR';

  // eslint-disable-next-line no-shadow
  const cols = rows[0].map((_, colIndex) => rows.map((row) => row[colIndex]));
  const delRow = rows[randomIntFromInterval(0, 4)];
  const delCol = cols[randomIntFromInterval(0, 4)];

  const directionRandom = randomIntFromInterval(1, 2);

  let winPattern;

  if (directionRandom === 1) {
    winPattern = delRow;
  } else {
    winPattern = delCol;
  }

  const generated = _.pullAll(bingoPicks, winPattern)
    .sort(() => 0.5 - Math.random())
    .slice(0, 31);

  return {
    board: rows,
    generated: _.union(winPattern, generated),
  };
};

const loseBingo = () => {
  const rows = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < 5; i++) {
    let current = [];
    if (rows.length > 0) {
      current = _.flattenDeep(rows);
    }

    rows[i] = _.filter(_.cloneDeep(bingoPicks), (x) => current.indexOf(x) < 0)
      .sort(() => 0.5 - Math.random())
      .slice(0, 5);
  }

  rows[2][2] = 'STAR';

  // eslint-disable-next-line no-shadow
  const cols = rows[0].map((_, colIndex) => rows.map((row) => row[colIndex]));
  const delRow = rows[randomIntFromInterval(0, 4)];
  const delCol = cols[randomIntFromInterval(0, 4)];

  const patternBreaker = _.uniq(_.union(delRow, delCol, ['STAR']));

  const generated = _.pullAll(bingoPicks, patternBreaker)
    .sort(() => 0.5 - Math.random())
    .slice(0, 36);

  return {
    board: rows,
    generated,
  };
};

const loseKeno = (payload) => {
  return kenoPicks
    .slice(1, 81)
    .filter(
      (x) =>
        !payload
          .sort(() => 0.5 - Math.random())
          .slice(0, randomIntFromInterval(0, 9))
          .includes(x)
    )
    .sort(() => 0.5 - Math.random())
    .slice(0, 11);
};

const winKeno = (payload) => {
  return payload;
};

const winBullBear = (prize, prizeList) => {
  const winnerItem = bullbearPicks.sort(() => 0.5 - Math.random()).slice(0, 1)[0];
  const rest = _.filter(bullbearPicks, (x) => x !== winnerItem).slice(0, 7);
  const restPrizes = _.filter(prizeList, (x) => x.prizeValue !== prize.PrizeValue);
  const prizeValue = prize.reduce((acc, item) => acc + item.prizeValue, 0);

  const res = Array.from({ length: 3 }).fill({
    type: winnerItem,
    value: prizeValue,
  });

  rest.map((item) => {
    const tempPrize = restPrizes.sort(() => 0.5 - Math.random()).slice(0, 1)[0];
    return res.push({
      type: item,
      value: tempPrize.prizeValue,
    });
  });

  return res.sort(() => 0.5 - Math.random());
};

const loseBullBear = (prizeList) => {
  const randoms = bullbearPicks.sort(() => 0.5 - Math.random());
  const res = [];

  randoms.map((item) => {
    const tempPrize = prizeList.sort(() => 0.5 - Math.random()).slice(0, 1)[0];
    return res.push({
      type: item,
      value: tempPrize.prizeValue,
    });
  });

  return res.sort(() => 0.5 - Math.random());
};

const loseWheel = () => {
  return {
    type: 0,
    value: 0,
  };
};

const winWheel = (prize, prizeList) => {
  const prizeIndex = _.findIndex(prizeList, (x) => x.prizeValue === prize[0].prizeValue);
  const prizeValue = prize.reduce((acc, item) => acc + item.prizeValue, 0);
  const bonusPrizes = prize.shift();
  return {
    type: prizeIndex,
    value: prizeValue,
    hasBonus: bonusPrizes.length > 0,
    bonusePrize: bonusPrizes.length > 0 ? bonusPrizes : [],
  };
};

const winSlot = () => {
  const winnerItem = slotPicks.sort(() => 0.5 - Math.random()).slice(0, 1)[0];

  return Array(3).fill(winnerItem);
};

const loseSlot = () => {
  return slotPicks.sort(() => 0.5 - Math.random()).slice(0, 3);
};

module.exports = {
  winRoulette,
  loseRoulette,
  winBingo,
  loseBingo,
  loseKeno,
  winKeno,
  winBullBear,
  loseBullBear,
  loseWheel,
  winWheel,
  winSlot,
  loseSlot,
};
