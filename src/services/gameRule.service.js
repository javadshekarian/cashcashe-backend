const httpStatus = require('http-status');
const _ = require('lodash');
const { GameRule } = require('../models');
const ApiError = require('../utils/ApiError');

const createGameRule = async (ruleBody) => {
  const _rule = new GameRule(ruleBody);

  await _rule.save();

  return _rule;
};

const deactivateCurrentRule = async (targetGame) => {
  const _currentRule = await GameRule.findOne({
    targetGame,
    isActive: true,
  });

  if (_currentRule) {
    _currentRule.isActive = false;
    _currentRule.markModified('isActive');
    await _currentRule.save();
  }
  return {
    success: true,
  };
};

const customRuleValidations = (rules) => {
  const wheelIndex = _.findIndex(rules, (x) => x.targetGame === 'WF');

  if (wheelIndex > -1) {
    if (rules[wheelIndex].rulePrizes.length !== 7) {
      throw new ApiError(httpStatus.OK, 'Rules for Wheel of furtune must be 7 items.');
    }
  }

  _.map(rules, (item) => {
    const zeroLimits = _.filter(item.rulePrizes, (x) => x.prizeLimit === 0);

    if (zeroLimits.length > 0) {
      throw new ApiError(httpStatus.OK, `Rules cannot include items with Play limits of 0`);
    }

    const zeroValues = _.filter(item.rulePrizes, (x) => x.prizeValue === 0);

    if (zeroValues.length > 0) {
      throw new ApiError(httpStatus.OK, `Rules cannot include items with Prize values of 0`);
    }

    _.map(item.rulePrizes, (prize) => {
      const duplicateLimit = _.filter(item.rulePrizes, (x) => x.prizeLimit === prize.prizeLimit);

      if (duplicateLimit.length > 1) {
        throw new ApiError(httpStatus.OK, `Rules cannot include items with the same Prize limits`);
      }

      return duplicateLimit;
    });

    _.map(item.rulePrizes, function (o, i) {
      const eq = _.find(item.rulePrizes, function (e, ind) {
        if (i > ind) {
          return _.isEqual(e, o);
        }
      });
      if (eq) {
        throw new ApiError(httpStatus.OK, `Rules cannot include duplicate items`);
      }
    });
  });
};

const getAllRules = async () => {
  const _rules = await GameRule.find({
    isActive: true,
  });

  return _rules;
};

module.exports = {
  createGameRule,
  getAllRules,
  deactivateCurrentRule,
  customRuleValidations,
};
