const httpStatus = require('http-status');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const ApiError = require('../utils/ApiError');
const userServices = require('./user.service');
const config = require('../config/config');
/**
 * Get profile transactions
 * @param {String} userEmail
 * @returns {Array<UserTransactions>}
 */
const getProfileTransactions = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { transactions } = user;

  if (transactions) {
    return transactions;
  }
  return [];
};
/**
 * Get profile deposits
 * @param {String} userEmail
 * @returns {Array<UserDeposits>}
 */
const getProfileDeposits = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { transactions } = user;

  if (transactions) {
    const depostis = _.filter(transactions, (x) => x.type === 1);

    return depostis;
  }
  return [];
};
/**
 * Get profile withdraws
 * @param {String} userEmail
 * @returns {Array<UserWithdraws>}
 */
const getProfileWithdraws = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { transactions } = user;

  if (transactions) {
    const withdraws = _.filter(transactions, (x) => x.type === 2);

    return withdraws;
  }
  return [];
};
/**
 * Get profile games
 * @param {String} userEmail
 * @returns {Array<UserGames>}
 */
const getProfileGames = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { games } = user;

  if (games) {
    return games;
  }
  return [];
};
/**
 * Get profile games
 * @param {String} userEmail
 * @returns {Object<DashboardInfo>}
 */
const getDashboardInfo = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { games, transactions } = user;

  const withdraws = _.filter(transactions, (x) => x.type === 2);
  const deposits = _.filter(transactions, (x) => x.type === 1);
  const wins = _.filter(games, (x) => x.win === true);
  const losts = _.filter(games, (x) => x.win === false);

  let balance = 0;
  let withdrawsSum = 0;
  let depositsSum = 0;

  // eslint-disable-next-line array-callback-return
  deposits.map((item) => {
    depositsSum += item.amount;
    balance += item.amount;
  });

  // eslint-disable-next-line array-callback-return
  losts.map((item) => {
    balance -= item.invest_amount;
  });

  // eslint-disable-next-line array-callback-return
  wins.map((item) => {
    balance += item.invest_amount;
  });

  // eslint-disable-next-line array-callback-return
  withdraws.map((item) => {
    withdrawsSum += item.amount;
    balance -= item.amount;
  });

  return {
    games: games.slice(0, 3),
    transactions: transactions.slice(0, 6),
    balance,
    deposits: depositsSum,
    withdraws: withdrawsSum,
  };
};

const getUserBalance = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { transactions } = user;

  const withdraws = _.filter(transactions, (x) => x.type === 2);
  const deposits = _.filter(transactions, (x) => x.type === 1);

  let balance = 0;
  let withdrawsSum = 0;
  let depositsSum = 0;

  // eslint-disable-next-line array-callback-return
  deposits.map((item) => {
    depositsSum += item.amount;
    balance += item.amount;
  });

  // eslint-disable-next-line array-callback-return
  withdraws.map((item) => {
    withdrawsSum += item.amount;
    balance -= item.amount;
  });

  return {
    balance,
    withdraws: withdrawsSum,
    deposits: depositsSum,
  };
};
/**
 * Get profile games
 * @param {String} userEmail
 * @returns {Object<UserDetail>}
 */
const getUserDetail = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { email, name, lastName, userName, profilePicture, country, phone, about, address, hasInfo } = user;

  return {
    email,
    name,
    lastName,
    userName,
    profilePic: profilePicture.url || '',
    country,
    phone,
    about,
    address,
    hasInfo: hasInfo || false,
  };
};

const getUserPaymentDestinations = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { customerAuthorizations } = user;

  if (!customerAuthorizations) {
    return [];
  }

  const arr = customerAuthorizations.reduce(
    (acc, i) =>
      _.concat(acc, {
        digits: i.authorization.last4,
        month: i.authorization.exp_month,
        year: i.authorization.exp_year,
        type: i.authorization.card_type,
        brand: i.authorization.brand,
        bank: i.authorization.bank,
        holder: i.authorization.account_name,
        ac: jwt.sign(i.authorization.authorization_code, config.jwt.secretTfa),
      }),
    []
  );

  return _.uniqWith(arr, _.isEqual);
};

const getUserWithdrawBalance = async (userEmail) => {
  const user = await userServices.getUserByEmail(userEmail);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const { transactions } = user;

  const withdraws = _.filter(transactions, (x) => x.type === 2);
  const deposits = _.filter(transactions, (x) => x.type === 1 && x.transactionInstance === 'GMI');

  let balance = 0;

  // eslint-disable-next-line array-callback-return
  deposits.map((item) => {
    balance += item.amount;
  });

  // eslint-disable-next-line array-callback-return
  withdraws.map((item) => {
    balance -= item.amount;
  });

  return {
    balance,
  };
};
module.exports = {
  getProfileTransactions,
  getProfileDeposits,
  getProfileWithdraws,
  getProfileGames,
  getDashboardInfo,
  getUserDetail,
  getUserBalance,
  getUserPaymentDestinations,
  getUserWithdrawBalance,
};
