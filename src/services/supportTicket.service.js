const { SupportTicket } = require('../models');
/**
 * Create a Support ticket
 * @param {Object} ticketBody
 * @returns {Promise<SupportTicket>}
 */
const createTicket = async (ticketBody) => {
  const ticket = new SupportTicket(ticketBody);
  await ticket.save();

  return ticket;
};

/**
 * Find a Support ticket by ticket number
 * @param {String} ticketNumber
 * @returns {Promise<SupportTicket>}
 */
const findTicketByNumber = async (ticketNumber) => {
  return SupportTicket.findOne({
    ticketNumber,
  });
};

module.exports = {
  createTicket,
  findTicketByNumber,
};
