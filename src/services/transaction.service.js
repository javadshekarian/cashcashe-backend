const httpStatus = require('http-status');
const axios = require('axios');
const Transaction = require('../models/transaction.model');
const GameTransaction = require('../models/gameTransaction.model');
const DailyTransaction = require('../models/dailyTransaction.model');
const MonthlyTransaction = require('../models/monthlyTransaction');
const ApiError = require('../utils/ApiError');
const config = require('../config/config');
const validateBySchema = require('../utils/validateBySchema');
const { verifyTransactionResponse } = require('../validations/transaction.validation');
const User = require('../models/user.model');
const Withdraw = require('../models/withdrawRequest.model');

const reportDailyTransaction = async (item) => {
  const _currentRecord = await DailyTransaction.findOne({
    date: new Date().toJSON().slice(0, 10),
  });

  if (!_currentRecord) {
    const _newTransaction = new DailyTransaction({
      date: new Date().toJSON().slice(0, 10),
      transactionCount: 1,
      actionCount: 1,
      changeLog: [
        {
          date: new Date().toLocaleString(),
          info: item.changeLog[0].info || '',
          invoiceNumber: item.invoiceNumber,
          status: item.status,
        },
      ],
      transactions: [item],
    });

    await _newTransaction.save();

    return true;
  }

  const _transactionIndex = _currentRecord.transactions.findIndex((x) => x.invoiceNumber === item.invoiceNumber);

  if (_transactionIndex < 0) {
    _currentRecord.changeLog.unshift({
      date: new Date().toLocaleString(),
      info: item.changeLog[0].info,
      invoiceNumber: item.invoiceNumber,
      status: item.status,
    });

    _currentRecord.transactions.unshift(item);

    await DailyTransaction.findOneAndUpdate(
      {
        date: new Date().toJSON().slice(0, 10),
      },
      {
        transactions: [..._currentRecord.transactions],
        changeLog: [..._currentRecord.changeLog],
        actionCount: _currentRecord.actionCount + 1,
        transactionCount: _currentRecord.transactionCount + 1,
      }
    );

    return true;
  }

  _currentRecord.transactions[_transactionIndex] = item;
  _currentRecord.markModified('transactions');
  _currentRecord.changeLog.unshift({
    date: new Date().toLocaleString(),
    info: item.changeLog[0].info,
    invoiceNumber: item.invoiceNumber,
    status: item.status,
  });
  _currentRecord.actionCount += 1;

  await _currentRecord.save();

  return true;
};

const reportMonthlyTransaction = async (item) => {
  const _currentRecord = await MonthlyTransaction.findOne({
    date: new Date().toJSON().slice(0, 7),
  });

  if (!_currentRecord) {
    const _newTransaction = new MonthlyTransaction({
      date: new Date().toJSON().slice(0, 7),
      transactionCount: 1,
      actionCount: 1,
      changeLog: [
        {
          date: new Date().toLocaleString(),
          info: item.changeLog[0].info || '',
          invoiceNumber: item.invoiceNumber,
          status: item.status,
        },
      ],
      transactions: [item],
    });

    await _newTransaction.save();

    return true;
  }

  const _transactionIndex = _currentRecord.transactions.findIndex((x) => x.invoiceNumber === item.invoiceNumber);

  if (_transactionIndex < 0) {
    _currentRecord.changeLog.unshift({
      date: new Date().toLocaleString(),
      info: item.changeLog[0].info,
      invoiceNumber: item.invoiceNumber,
      status: item.status,
    });

    _currentRecord.transactions.unshift(item);

    await MonthlyTransaction.findOneAndUpdate(
      {
        date: new Date().toJSON().slice(0, 7),
      },
      {
        transactions: [..._currentRecord.transactions],
        changeLog: [..._currentRecord.changeLog],
        actionCount: _currentRecord.actionCount + 1,
        transactionCount: _currentRecord.transactionCount + 1,
      }
    );

    return true;
  }

  _currentRecord.transactions[_transactionIndex] = item;
  _currentRecord.markModified('transactions');
  _currentRecord.changeLog.unshift({
    date: new Date().toLocaleString(),
    info: item.changeLog[0].info,
    invoiceNumber: item.invoiceNumber,
    status: item.status,
  });
  _currentRecord.actionCount += 1;

  await _currentRecord.save();

  return true;
};

const createTransaction = async ({
  type,
  amount,
  absAmount,
  fees,
  currency,
  reference,
  assocUser,
  event,
  status,
  paymentPayload,
  paymentVerificationResponse,
  transactionInstance,
}) => {
  const _transaction = new Transaction({
    type,
    amount,
    transactionInstance: transactionInstance || 'IPG',
    absAmount: absAmount || 0,
    fees: fees || 0,
    assocUser,
    currency,
    reference,
    changeLog: event
      ? [
          event,
          {
            date: new Date().toLocaleString(),
            info: 'Transaction has been created by the user',
            status: status || 1,
          },
        ]
      : [
          {
            date: new Date().toLocaleString(),
            info: 'Transaction has been created by the user',
            status: status || 1,
          },
        ],
    status: status || 1,
    paymentPayload: paymentPayload || {},
    paymentVerificationResponse: paymentVerificationResponse || {},
  });
  await _transaction.save();

  const _user = await User.findById(assocUser);

  let userTransactions = _user.transactions;

  if (!userTransactions) userTransactions = [];

  userTransactions.unshift(_transaction.toJSON());

  _user.transactions = [...userTransactions];

  _user.markModified('transactions');

  await _user.save();

  await reportDailyTransaction(_transaction);

  await reportMonthlyTransaction(_transaction);

  await _transaction.save();

  return _transaction;
};

const createGameTransaction = async ({ type, amount, assocUser, targetGameId, appliedBy }) => {
  const _transaction = new GameTransaction({
    type,
    amount,
    assocUser,
    targetGameId,
    transactionInstance: 'GMI',
    appliedBy: appliedBy || assocUser,
  });

  await _transaction.save();

  const _user = await User.findById(assocUser);

  let userTransactions = _user.transactions;

  if (!userTransactions) userTransactions = [];

  userTransactions.unshift(_transaction.toJSON());

  _user.transactions = [...userTransactions];

  _user.markModified('transactions');

  await _user.save();

  await reportDailyTransaction(_transaction);

  await reportMonthlyTransaction(_transaction);

  await _transaction.save();

  return _transaction;
};

const updateTransactionToPayStatus = async ({ tid }) => {
  const _transaction = await Transaction.findById(tid);

  if (!_transaction) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Transaction not found');
  }

  _transaction.status = 2;
  _transaction.changeLog.unshift({
    date: new Date().toLocaleString(),
    info: 'Transaction has been transfered to the payment gateway',
    status: 2,
  });

  await _transaction.save();

  await reportDailyTransaction(_transaction);
  await reportMonthlyTransaction(_transaction);

  return true;
};

const findTransactionByReference = async (reference) => {
  const _transaction = await Transaction.findOne({ reference });

  return _transaction;
};

const validateTransactionPayload = (payload) => {
  const { reference, trans, transaction, trxref } = payload;

  if (Number(reference) === Number(trxref) && Number(trans) === Number(transaction)) {
    return {
      isValid: true,
    };
  }

  return {
    isValid: false,
  };
};

const verifyTransactionByReference = async (ref) => {
  const response = await axios({
    url: `https://api.paystack.co/transaction/verify/${ref}`,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${config.paystack_secret}`,
    },
  });

  const { data, status } = response;

  const { isValid, message } = validateBySchema(verifyTransactionResponse, {
    status,
    data,
  });

  if (isValid) {
    return {
      success: true,
      amount: (data.data.amount - data.data.fees) / 100,
      absAmount: data.data.amount / 100,
      fees: data.data.fees / 100,
      currency: data.data.currency,
      message: data.message,
      verificationResponse: response.data,
    };
  }

  return {
    success: false,
    amount: response.data && response.data.data && response.data.data.amount ? response.data.data.amount / 100 : 0,
    currency: response.data && response.data.data && response.data.data.currency ? response.data.data.currency : 0,
    message,
    verificationResponse: response.data,
  };
};

const createWithdrawTransaction = async ({ amount, assocUser, customerAuthorization }) => {
  const withdraw = new Withdraw({
    amount,
    assocUser,
    customerAuthorization,
  });

  await withdraw.save();

  return withdraw;
};

module.exports = {
  createTransaction,
  reportDailyTransaction,
  reportMonthlyTransaction,
  updateTransactionToPayStatus,
  findTransactionByReference,
  validateTransactionPayload,
  verifyTransactionByReference,
  createGameTransaction,
  createWithdrawTransaction,
};
