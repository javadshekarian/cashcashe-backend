const Sentry = require('@sentry/node');
const logger = require('../config/logger');

const catchAsync = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch((err) => {
    Sentry.captureException(err);
    logger.warn('Exception Captured');
    next(err);
  });
};

module.exports = catchAsync;
