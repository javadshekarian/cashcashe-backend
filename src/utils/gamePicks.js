const _ = require('lodash');

const kenoPicks = Array.from(Array(81).keys()).slice(1, 81);

const bingoPicks = Array.from(Array(76).keys()).slice(1, 76);

const bullbearPicks = ['TRENDUP', 'EXCHANGE', 'BULLUP', 'COINSTACK', 'DRAW', 'HODL', 'BEARDOWN', 'DRAWBULL', 'TRENDDOWN'];

const slotPicks = Array.from(Array(10).keys()).slice(1, 10);

const rouletteNumbers = Array.from(Array(37).keys()).slice(0, 37);

const roulette = {
  all: _.union(Array.from(Array(37).keys()).slice(0, 37), [
    'FR',
    'SR',
    'TR',
    'F12',
    'S12',
    'T12',
    '1T18',
    'EVEN',
    'RED',
    'BLACK',
    'ODD',
    '19T36',
    'VDZ',
    'TIERS',
    'ORP',
  ]),
  mods: ['FR', 'SR', 'TR', 'F12', 'S12', 'T12', '1T18', 'EVEN', 'RED', 'BLACK', 'ODD', '19T36', 'VDZ', 'TIERS', 'ORP'],
  numbers: rouletteNumbers,
  VDZ: [22, 18, 29, 7, 28, 12, 35, 3, 26, 0, 32, 15, 19, 4, 21, 2, 25],
  TIERS: [27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33],
  ORP: [17, 34, 6, 1, 20, 14, 31, 9],
  EVEN: rouletteNumbers.filter((x) => x % 2 === 0),
  ODD: rouletteNumbers.filter((x) => x % 2 !== 0),
  BLACK: [2, 6, 4, 8, 11, 10, 15, 13, 17, 20, 24, 22, 26, 29, 28, 33, 31, 36],
  RED: [3, 5, 1, 9, 7, 12, 14, 18, 16, 21, 19, 23, 27, 25, 30, 32, 35, 34],
  '1T18': rouletteNumbers.slice(1, 19),
  '19T36': rouletteNumbers.slice(19, 37),
  F12: rouletteNumbers.slice(1, 13),
  S12: rouletteNumbers.slice(12, 25),
  T12: rouletteNumbers.slice(24, 37),
  FR: [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36],
  SR: [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35],
  TR: [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34],
};

module.exports = {
  kenoPicks,
  bingoPicks,
  bullbearPicks,
  roulette,
  slotPicks,
};
