/* eslint-disable no-await-in-loop */
const mongoose = require('mongoose');
const config = require('../config/config');
const logger = require('../config/logger');
const { Game } = require('../models');
const { gameService } = require('../services');
const sleep = require('./sleep');
const { getThreadTaskCount, getThreadOnline } = require('../services/localDb.service');

mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {});

async function gameResultCalculator() {
  let _games = await Game.find({
    isFinished: false,
  });

  await gameService.handleResults(_games);

  let active = true;

  while (active) {
    const threadHasTask = await getThreadTaskCount();
    const threadOnline = await getThreadOnline();

    if (!threadOnline) {
      active = false;
    }
    if (threadHasTask > 0) {
      logger.info('Starting Cycle');
      _games = await Game.find({
        isFinished: false,
      });

      await gameService.handleResults(_games);
      logger.info('Cycle Ended');
    }

    await sleep(1000);
  }
}

process.on('message', async () => {
  await gameResultCalculator();
  process.send('Thread Has been shut down by local db command');
});
