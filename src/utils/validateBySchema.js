const Joi = require('joi');

const validateBySchema = (validSchema, object) => {
  const { error } = Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' }, abortEarly: false })
    .validate(object);

  if (error) {
    return {
      isValid: false,
      message: error,
    };
  }

  return {
    isValid: true,
    message: null,
  };
};

module.exports = validateBySchema;
