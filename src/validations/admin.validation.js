const Joi = require('joi');
const config = require('../config/config');
const { roles } = require('../config/roles');
const { objectId, country, address, about, password } = require('./custom.validation');
const { TICKET_ISSUE_TYPES } = require('./profile.validation');

const queryTransactions = {
  body: Joi.object()
    .keys({
      filter: Joi.object(),
      options: Joi.object().keys({
        sortBy: Joi.string(),
        populate: Joi.string(),
        limit: Joi.number(),
        page: Joi.number(),
      }),
    })
    .min(1)
    .required(),
};

const createSetting = {
  body: Joi.object()
    .keys({
      maintenanceMode: Joi.bool(),
      siteLogo: Joi.string(),
      siteName: Joi.string(),
      siteDescription: Joi.string(),
    })
    .required(),
};

const debitCreditUser = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      instance: Joi.any()
        .label('Transaction Instance')
        .valid(...config.transactionInstanctes)
        .required(),
      amount: Joi.number().required(),
    })
    .required(),
};

const updateUser = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string().description(`User's given name`).allow(''),
      lastName: Joi.string().description(`User's lastname`).allow(''),
      country: Joi.string().description(`User's living country`).custom(country).allow(''),
      phone: Joi.number().description(`User's phone number`).allow(''),
      address: Joi.string().description(`User's address`).custom(address).allow(''),
      about: Joi.string().description(`User about`).custom(about).allow(''),
      profilePic: Joi.string().uri().description(`User's Profile picture`).allow(''),
      role: Joi.any()
        .valid(...roles)
        .description(`User's Role`),
      agents: Joi.array().description(`User's Agent list`),
    })
    .min(1),
};

const userBan = {
  params: Joi.object().keys({
    userId: Joi.required().custom(objectId),
  }),
};

const viewSupportTicket = {
  params: Joi.object().keys({
    tid: Joi.string().required().custom(objectId),
  }),
};

const setMasterPassowrd = {
  body: Joi.object().keys({
    currentPassword: Joi.string().required().label('Current Master Password'),
    password: Joi.string().required().label('New Master Password').custom(password),
  }),
};

const createTicket = {
  body: Joi.object().keys({
    receiver: Joi.string().email().required().description('Receiver Email Address'),
    subject: Joi.string().required().description('Ticket subject').min(5),
    issueType: Joi.any()
      .required()
      .valid(...TICKET_ISSUE_TYPES.map((item) => item.value))
      .description('Issue Category'),
    description: Joi.string().required().min(2),
  }),
};

const answerTicket = {
  param: Joi.object().keys({
    ticketNumber: Joi.string().required().description('Ticket number'),
  }),
  body: Joi.object().keys({
    description: Joi.string().required().min(5).description('Message'),
  }),
};

const closeTicket = {
  param: Joi.object().keys({
    ticketNumber: Joi.string().required().description('Ticket number'),
  }),
};

const getUserInfo = {
  param: Joi.object().keys({
    userId: Joi.string().required().custom(objectId),
  }),
};

module.exports = {
  queryTransactions,
  createSetting,
  debitCreditUser,
  updateUser,
  userBan,
  viewSupportTicket,
  setMasterPassowrd,
  createTicket,
  answerTicket,
  closeTicket,
  getUserInfo,
};
