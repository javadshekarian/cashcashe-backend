const Joi = require('joi');
const config = require('../config/config');

const creditUser = {
  body: Joi.object().keys({
    userEmail: Joi.string().required().email().label(`User email`),
    amount: Joi.number().required().label('Transaction amount'),
  }),
};

const playTokenUsers = {
  body: Joi.object().keys({
    email: Joi.string().required(),
  }),
};

const getPlayToken = {
  body: Joi.object().keys({
    email: Joi.string().required(),
    targetGame: Joi.any()
      .required()
      .valid(...config.games.gameIDs.map((item) => item)),
  }),
};

const queryTransactions = {
  body: Joi.object()
    .keys({
      filter: Joi.object(),
      options: Joi.object().keys({
        sortBy: Joi.string(),
        populate: Joi.string(),
        limit: Joi.number(),
        page: Joi.number(),
      }),
    })
    .min(1)
    .required(),
};

module.exports = {
  creditUser,
  playTokenUsers,
  getPlayToken,
  queryTransactions,
};
