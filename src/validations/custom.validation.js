const _ = require('lodash');
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const countryOptions = require('../utils/countryOptions');

const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo id');
  }
  return value;
};

const password = (value, helpers) => {
  if (value.length < 8) {
    return helpers.message('password must be at least 8 characters');
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message('password must contain at least 1 letter and 1 number');
  }
  return value;
};

const country = (value, helpers) => {
  const _country = _.filter([...countryOptions], (x) => x.code === value);
  if (_country.length < 1) {
    return helpers.message(`Country Code ${value} is not supported`);
  }
  return value;
};

const phone = (obj, helpers) => {
  const isValid = phoneUtil.isValidNumberForRegion(phoneUtil.parse(obj.phone, obj.country), obj.country);
  if (!isValid) {
    return helpers.message('Invalid phone number');
  }
  return obj;
};

const address = (value, helpers) => {
  const isValid = value.split(' ').length > 3;
  if (!isValid) {
    return helpers.message('Address must contain at least 4 words');
  }
  return value;
};

const about = (value, helpers) => {
  const isValid = value.split(' ').length >= 5;
  if (!isValid) {
    return helpers.message('About you field must contain at least 5 words');
  }
  return value;
};

module.exports = {
  objectId,
  password,
  country,
  phone,
  address,
  about,
};
