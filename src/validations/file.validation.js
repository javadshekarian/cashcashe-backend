const Joi = require('joi');
const { objectId } = require('./custom.validation');

const uploadFile = {
  body: Joi.object().keys({
    file: Joi.any().required(),
  }),
};

const viewFile = {
  params: Joi.object().keys({
    fileId: Joi.string().label('File key').required().custom(objectId),
    fileName: Joi.string().label('File name').required(),
  }),
};

module.exports = {
  uploadFile,
  viewFile,
};
