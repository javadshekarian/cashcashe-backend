const Joi = require('joi');
const { objectId } = require('./custom.validation');
const { kenoPicks, roulette } = require('../utils/gamePicks');

const playKeno = {
  body: Joi.object()
    .keys({
      payload: Joi.array()
        .label('User picks')
        .min(10)
        .max(10)
        .items(...kenoPicks)
        .required(),
    })
    .required(),
  query: Joi.object().keys({
    playToken: Joi.string().custom(objectId),
  }),
};

const playBullBear = {
  query: Joi.object().keys({
    playToken: Joi.string().custom(objectId),
  }),
};

const playRoulette = {
  body: Joi.object()
    .keys({
      payload: Joi.any()
        .label('User picks')
        .valid(...roulette.all)
        .required(),
    })
    .required(),
  query: Joi.object().keys({
    playToken: Joi.string().custom(objectId),
  }),
};

const playBingo = {
  query: Joi.object().keys({
    playToken: Joi.string().custom(objectId),
  }),
};

const playWheel = {
  query: Joi.object().keys({
    playToken: Joi.string().custom(objectId),
  }),
};

const playSlot = {
  query: Joi.object().keys({
    playToken: Joi.string().custom(objectId),
  }),
};
const getGameStatus = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId).required().label('Game ID'),
  }),
};

module.exports = {
  playKeno,
  playBullBear,
  playRoulette,
  playBingo,
  playWheel,
  playSlot,
  getGameStatus,
};
