const Joi = require('joi');
const config = require('../config/config');
const { password } = require('./custom.validation');

const setRules = {
  body: Joi.object()
    .keys({
      password: Joi.string().required(),
      rules: Joi.array()
        .label('Rules list')
        .items(
          Joi.object()
            .label('Rule item')
            .keys({
              targetGame: Joi.string()
                .valid(...config.games.gameIDs)
                .required()
                .label('Target game'),
              rulePrizes: Joi.array()
                .label('Prize list')
                .items(
                  Joi.object().keys({
                    prizeLabel: Joi.string().required().label('Prize Label'),
                    prizeValue: Joi.number().required().label('Prize Value'),
                    prizeLimit: Joi.number().required().label('Prize Limit'),
                  })
                ),
            })
        ),
    })
    .required(),
};

const getRules = {
  params: Joi.object().keys({
    password: Joi.string().required(),
  }),
};

module.exports = {
  setRules,
  getRules,
};
