const Joi = require('joi');
const { country, address, about } = require('./custom.validation');

const TICKET_ISSUE_TYPES = [
  {
    label: 'Deposit',
    value: 'DEP',
  },
  {
    label: 'Withdraw',
    value: 'WITH',
  },
  {
    label: 'Accounts',
    value: 'ACC',
  },
];

const updateProfile = {
  body: Joi.object().keys({
    name: Joi.string().required().description(`User's given name`),
    lastName: Joi.string().required().description(`User's lastname`),
    country: Joi.string().description(`User's living country`).custom(country).allow(''),
    phone: Joi.number().description(`User's phone number`).allow(''),
    address: Joi.string().description(`User's address`).custom(address).allow(''),
    about: Joi.string().description(`User about`).custom(about).allow(''),
    profilePic: Joi.string().uri().description(`User's Profile picture`).allow(''),
  }),
};

const createTicket = {
  body: Joi.object().keys({
    subject: Joi.string().required().description('Ticket subject').min(5),
    issueType: Joi.any()
      .required()
      .valid(...TICKET_ISSUE_TYPES.map((item) => item.value)),
    description: Joi.string()
      .required()
      .custom((value, helpers) => {
        if (value.split(' ').length < 5) {
          return helpers.message('Ticket subject must contain at least 5 words');
        }
        return value;
      }),
  }),
};

const viewTicket = {
  param: Joi.object().keys({
    ticketNumber: Joi.string().required().description('Ticket number'),
  }),
};

const answerTicket = {
  param: Joi.object().keys({
    ticketNumber: Joi.string().required().description('Ticket number'),
  }),
  body: Joi.object().keys({
    description: Joi.string().required().min(2).description('Message'),
  }),
};

const closeTicket = {
  param: Joi.object().keys({
    ticketNumber: Joi.string().required().description('Ticket number'),
  }),
};

const otp = {
  body: Joi.object().keys({
    token: Joi.string().required().min(6).max(6).description('OTP Token'),
  }),
};

const requestWithdraw = {
  body: Joi.object().keys({
    amount: Joi.number().required().min(100).description('Withdraw amount'),
    ac: Joi.string().required().description('Payment Method'),
  }),
};

const viewWithdrawRequest = {
  param: Joi.object().keys({
    wid: Joi.string().required().description('Payment Method'),
  }),
};

const confirmWithdrawRequest = {
  param: Joi.object().keys({
    wid: Joi.string().required().description('Payment Method'),
  }),
  body: Joi.object().keys({
    token: Joi.string().required().min(6).max(6).description('OTP Token'),
  }),
};

module.exports = {
  updateProfile,
  createTicket,
  viewTicket,
  answerTicket,
  closeTicket,
  TICKET_ISSUE_TYPES,
  otp,
  requestWithdraw,
  viewWithdrawRequest,
  confirmWithdrawRequest,
};
