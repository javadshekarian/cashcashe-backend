const Joi = require('joi');

const paymentTransaction = {
  body: Joi.object().keys({
    amount: Joi.number().min(100).required().description('Payment amount'),
  }),
};

const topupAccount = {
  body: Joi.object().keys({
    message: Joi.string().required().allow('Approved'),
    redirecturl: Joi.string().required(),
    reference: Joi.string().required(),
    status: Joi.string().required().allow('success'),
    trans: Joi.number().required(),
    transaction: Joi.number().required(),
    trxref: Joi.number().required(),
  }),
};

const verifyTransactionResponse = {
  status: Joi.number().required().valid(200).description('Verification Request status'),
  data: Joi.object().keys({
    status: Joi.boolean().required().valid(true).description('Request response state'),
    message: Joi.string().required(),
    data: Joi.object().keys({
      id: Joi.number().required(),
      domain: Joi.string().required(),
      status: Joi.string().required().valid('success'),
      reference: Joi.number().required(),
      amount: Joi.number().required(),
      message: Joi.any(),
      gateway_response: Joi.string().required(),
      paid_at: Joi.date().required(),
      created_at: Joi.date().required(),
      channel: Joi.string().required(),
      currency: Joi.string().required(),
      ip_address: Joi.string().required(),
      metadata: Joi.object(),
      log: Joi.object(),
      fees: Joi.number().required(),
      fees_split: Joi.any(),
      authorization: Joi.object().required(),
      customer: Joi.object().required(),
      plan: Joi.any(),
      split: Joi.object(),
      order_id: Joi.any(),
      paidAt: Joi.string().required(),
      createdAt: Joi.string().required(),
      requested_amount: Joi.number().required(),
      pos_transaction_data: Joi.any(),
      source: Joi.any(),
      fees_breakdown: Joi.any(),
      transaction_date: Joi.date().required(),
      plan_object: Joi.any(),
      subaccount: Joi.any(),
    }),
  }),
};

module.exports = {
  paymentTransaction,
  topupAccount,
  verifyTransactionResponse,
};
